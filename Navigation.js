import React from 'react';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';

import HomeScreen from './screen/home/HomeScreen';
import SendScreen from './screen/send/SendScreen';
import ReceiveScreen from './screen/receive/ReceiveScreen';
import ContactsScreen from './screen/contacts/ContactsScreen';
import TransactionsScreen from './screen/transactions/TransactionsScreen';
import SeedScreen from './screen/seed/SeedScreen';
import ConvertScreen from './screen/convert/ConvertScreen';
import WelcomeScreen from './screen/welcome/WelcomeScreen';
import ImportWalletScreen from './screen/import/ImportWalletScreen';
import SettingsScreen from './screen/settings/SettingsScreen';
import UnlockScreen from './screen/unlock/UnlockScreen';

import 'react-native-gesture-handler';
import {createDrawerNavigator} from '@react-navigation/drawer';
import BambooDrawer from './components/BambooDrawer';
import colors, {themes} from './styles/colors';

import {
  Cog,
  Convert,
  Home,
  Key,
  Magnifier,
  User,
  WalletDrawer,
} from './components/BambooSvgs';
import CreatePasswordScreen from './screen/password/CreatePassword';
import UnlockWith from './UnlockWith';
import {ThemeContext} from './contexts/ThemeStore';
import AsyncStorage from '@react-native-async-storage/async-storage';
import WalletsScreen from './screen/wallets/WalletsScreen';
import ScanQRCodeScreen from './screen/scan/ScanQRCodeScreen';
import SecurityScreen from './screen/settings/SecurityScreen';

global.Buffer = global.Buffer || require('buffer').Buffer;

const Drawer = createDrawerNavigator();

const IntroNav = createNativeStackNavigator();
const UnlockNav = createNativeStackNavigator();
const RootNav = createNativeStackNavigator();

const IntroStack = () => {
  return (
    <IntroNav.Navigator
      initialRouteName="Welcome"
      screenOptions={{headerShown: false}}>
      <IntroNav.Screen name="Welcome" component={WelcomeScreen} />
      <IntroNav.Screen name="Import" component={ImportWalletScreen} />
      <IntroNav.Screen name="CreatePass" component={CreatePasswordScreen} />
      <IntroNav.Screen name="ScanQr" component={ScanQRCodeScreen} />
    </IntroNav.Navigator>
  );
};

const HomeStack = () => {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <Drawer.Navigator
      drawerContent={props => <BambooDrawer {...props} />}
      initialRouteName="Home"
      backBehavior="initialRoute"
      hideStatusBar
      lazy
      drawerContentOptions={{
        activeBackgroundColor: colors.drawerActiveTint,
      }}
      drawerType="front"
      screenOptions={{
        headerShown: false,
      }}>
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        options={{
          drawerIcon: () => <Home />,
        }}
      />
      <Drawer.Screen
        name="Wallets"
        component={WalletsScreen}
        options={{
          headerShown: false,
          drawerIcon: () => <WalletDrawer />,
        }}
      />
      <Drawer.Screen
        name="Contacts"
        component={ContactsScreen}
        options={{
          drawerIcon: () => <User />,
        }}
      />
      <Drawer.Screen
        name="Transactions"
        component={TransactionsScreen}
        options={{
          drawerIcon: () => <Magnifier />,
        }}
      />
      <Drawer.Screen
        name="Convert"
        component={ConvertScreen}
        options={{
          drawerIcon: () => <Convert />,
        }}
      />
      <Drawer.Screen
        name="Seed Phrase"
        component={SeedScreen}
        options={{
          drawerIcon: () => <Key />,
        }}
      />
      <Drawer.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          drawerIcon: () => <Cog />,
        }}
      />
      <Drawer.Screen
        name="Security"
        component={SecurityScreen}
        options={{
          headerShown: false,
        }}
      />
      <Drawer.Screen
        name="Send"
        component={SendScreen}
        options={{
          headerShown: false,
        }}
      />
      <Drawer.Screen
        name="Receive"
        component={ReceiveScreen}
        options={{
          headerShown: false,
        }}
      />
      <Drawer.Screen
        name="ScanQr"
        component={ScanQRCodeScreen}
        options={{
          headerShown: false,
        }}
      />
    </Drawer.Navigator>
  );
};

const UnlockStack = () => (
  <UnlockNav.Navigator initialRouteName="Unlock">
    <UnlockNav.Screen
      name="Unlock"
      options={{headerShown: false}}
      component={UnlockScreen}
      initialParams={{unlockOnComponentMount: true}}
    />
  </UnlockNav.Navigator>
);

const RootStack = () => {
  return (
    <RootNav.Navigator
      initialRouteName="UnlockStack"
      screenOptions={{headerShown: false}}>
      <RootNav.Screen name="HomeStack" component={HomeStack} />
      <RootNav.Screen name="IntroStack" component={IntroStack} />
      <RootNav.Screen name="UnlockStack" component={UnlockStack} />
    </RootNav.Navigator>
  );
};

export default RootStack;
