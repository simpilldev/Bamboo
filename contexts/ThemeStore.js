import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Appearance} from 'react-native';

const ThemeContext = React.createContext(); // line A - creating the context

const ThemeStore = ({children}) => {
  const [defaultValue, setDefaultValue] = useState(
    Appearance.getColorScheme() === 'dark',
  );

  useEffect(() => {
    getDefaultValue().then(val => {
      setDarkMode(val);
    });
  }, []);

  const [isDarkMode, setDarkMode] = useState(Appearance.getColorScheme() === 'dark'); // line B - setting the initial theme
  // line C - changing the theme

  return (
    <ThemeContext.Provider value={{setDarkMode, isDarkMode}}>
      {children}
    </ThemeContext.Provider>
  );
};

export const getDefaultValue = async () => {
  let asyncStorageDarkMode = Appearance.getColorScheme() === 'dark';
  let theme = await AsyncStorage.getItem('BambooTheme');

  if (theme === 'dark') asyncStorageDarkMode = true;
  else if (theme === 'light') asyncStorageDarkMode = false;

  if (typeof asyncStorageDarkMode === 'boolean') return asyncStorageDarkMode;
};

export const saveTheme = theme => {
  AsyncStorage.setItem('BambooTheme', theme);
};

export {ThemeStore, ThemeContext};
