package io.winston.bamboo;

import android.content.pm.ActivityInfo;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import android.os.PersistableBundle;

import android.content.res.Configuration;
import android.view.View;
import androidx.core.view.ViewCompat;
import android.view.WindowManager;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;
import androidx.core.content.res.ResourcesCompat;

import androidx.annotation.Nullable;

import org.devio.rn.splashscreen.SplashScreen;

import com.facebook.react.ReactActivity;
import android.os.Bundle;
import com.zoontek.rnbootsplash.RNBootSplash;
// import com.zoontek.rnbars.RNBars;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "BlueWallet";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        RNBootSplash.init(this);
        super.onCreate(savedInstanceState);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        hideSystemUI();
        // RNBars.init(this, "dark-content");
    }

    private void hideSystemUI() {

        WindowInsetsControllerCompat windowInsetsController = ViewCompat
                .getWindowInsetsController(getWindow().getDecorView());
        if (windowInsetsController == null) {
            return;
        }
        windowInsetsController.setSystemBarsBehavior(
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
        // Hide both the status bar and the navigation bar
        windowInsetsController.hide(WindowInsetsCompat.Type.statusBars());

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS,
        // WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
    }

    private void setSystemBarColors() {
        // getWindow().setStatusBarColor(ResourcesCompat.getColor(getResources(),
        // R.color.green, null));
        // getWindow().setNavigationBarColor(ContextCompat.getColor(this,
        // R.color.green));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            hideSystemUI();
        }
    }
}
