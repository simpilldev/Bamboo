import * as React from 'react';
import {StyleSheet} from 'react-native';
import {ThemeContext} from '../contexts/ThemeStore';
import {themes} from './colors';

export const BambooTextStyles = () => {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return StyleSheet.create({
    titleTextStyle: {
      fontFamily: 'Montserrat-SemiBold',
      fontSize: 36,
      color: colors.primaryTextColor,
    },

    regularTextStyle: {
      fontFamily: 'Montserrat-Regular',
      fontSize: 20,
      color: colors.primaryTextColor,
    },

    smallTextStyle: {
      fontFamily: 'Montserrat-Regular',
      fontSize: 16,
      color: colors.primaryTextColor,
    },

    pndTickerStyle: {
      fontFamily: 'Montserrat-SemiBold',
      fontSize: 32,
      color: colors.pndTickerColor,
    },

    pndBalanceStyle: {
      fontFamily: 'Montserrat-Medium',
      fontSize: 48,
      color: colors.primaryTextColor,
    },

    footerTextStyle: {
      color: colors.primaryTextColor,
      fontFamily: 'Montserrat-Medium',
      letterSpacing: 0.2,
      marginTop: 24,
      paddingRight: 2,
      fontSize: 18,
    },

    modalTextTitleStyle: {
      color: colors.primaryTextColor,
      textAlign: 'center',
      fontFamily: 'Montserrat-SemiBold',
      fontSize: 28,
      marginVertical: 12,
      marginLeft: 32,
    },

    toastModalTextTitleStyle: {
      color: colors.modalTextColor,
      fontFamily: 'Montserrat-SemiBold',
      fontSize: 20,
    },

    toastModalTextBodyStyle: {
      fontFamily: 'Montserrat-Regular',
      fontSize: 14,
      color: colors.modalTextColor,
    },

    modalBodyTextStyle: {
      fontFamily: 'Montserrat-Regular',
      fontSize: 16,
      color: colors.modalTextColor,
      backgroundColor: colors.modalBackground,
      paddingHorizontal: 32,
      paddingVertical: 32,
    },

    modalBodyTextStyleNoBottomPadding: {
      fontFamily: 'Montserrat-Regular',
      fontSize: 16,
      color: colors.modalTextColor,
      backgroundColor: 'white',
      paddingHorizontal: 32,
      paddingTop: 32,
    },

    drawerItemTextStyle: {
      marginLeft: -10,
      fontFamily: 'Montserrat-Regular',
      fontSize: 15,
    },

    currencyModalTextStyle: {
      fontFamily: 'Montserrat-Regular',
      fontSize: 16,
      color: colors.gray27,
      backgroundColor: colors.modalBackground,
      paddingHorizontal: 32,
    },
  });
};
