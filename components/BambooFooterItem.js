import React, {useContext, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import colors, {themes} from '../styles/colors';
import LocalizedStrings from 'react-native-localization';
import {
  Add,
  BigQrCode,
  Coins,
  Copy,
  QrCode,
  Wallet,
  WalletReceive,
  WalletSend,
} from './BambooSvgs';
import {
  initialWindowMetrics,
  SafeAreaView,
  useSafeAreaInsets,
} from 'react-native-safe-area-context';
import LottieView from 'lottie-react-native';
import {ThemeContext} from '../contexts/ThemeStore';
import {BambooTextStyles, textStyles} from '../styles/textStyles';
import {BlueStorageContext} from '../blue_modules/storage-context';
import {HDSegwitBech32Wallet} from '../class';

const localeFile = require('../locale.json');
const strings = new LocalizedStrings(localeFile);

export default function BambooFooterItem({children, onPress}) {
  return (
    <SafeAreaView
      style={{
        marginBottom: 16,
        alignItems: 'center',
      }}>
      <TouchableOpacity
        style={{flexDirection: 'column', alignItems: 'center'}}
        onPress={onPress}>
        {children}
      </TouchableOpacity>
    </SafeAreaView>
  );
}

function BambooButton({text, onPress, isFooter, props}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={{
        minWidth: 120,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: isFooter
          ? initialWindowMetrics.insets.bottom === 0
            ? 64
            : initialWindowMetrics.insets.bottom * 1.5
          : 0,
        paddingVertical: 18,
        paddingHorizontal: 24,
        borderRadius: 10,
        backgroundColor: colors.buttonColor,
        shadowColor: colors.shadowColor,
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.17,
        shadowRadius: 2,
        elevation: 2.5,
      }}
      onPress={onPress}
      {...props}>
      <Text
        style={{
          color: colors.buttonTextColor,
          fontFamily: 'Montserrat-Regular',
          letterSpacing: 0.1,
          fontSize: 18,
        }}>
        {text}
      </Text>
    </TouchableOpacity>
  );
}

export function AddWalletButton({text, toggleModal}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;
  const {addAndSaveWallet} = React.useContext(BlueStorageContext);

  return (
    <BambooButton
      isFooter
      text={strings.screens.walletsScreen.addWalletButton}
      onPress={async () => {
        toggleModal();
      }}
    />
  );
}

export function CreateTxButton({
  navigation,
  toggleModal,
  toggleInvalidAddressModal,
  isVisible,
  areBoxesEmpty,
  isValid,
}) {
  return (
    <BambooButton
      isFooter
      text="Send"
      onPress={() => {
        if (!areBoxesEmpty() && isValid()) {
          toggleModal();
        }
      }}
    />
  );
}

export function CancelQrScanButton({navigation, props, onPress}) {
  return <BambooButton isFooter text="Back" onPress={onPress} props={props} />;
}

export function AddContactButton({toggleModal, isFooter}) {
  return (
    <BambooButton
      isFooter={isFooter}
      text={strings.screens.contactsScreen.addButton}
      onPress={() => {
        toggleModal();
      }}
    />
  );
}

export function SelectCurrencyButton({toggleModal}) {
  return (
    <BambooButton
      text={strings.screens.convertScreen.convert}
      onPress={() => {
        toggleModal();
      }}
    />
  );
}

export function SendButton({navigation}) {
  return (
    <BambooFooterItem
      onPress={() => {
        navigation.navigate('Send');
      }}>
      <WalletSend />
      <Text style={BambooTextStyles().footerTextStyle}>
        {strings.screens.homeScreen.send}
      </Text>
    </BambooFooterItem>
  );
}

export function ReceiveButton({navigation, address}) {
  return (
    <BambooFooterItem
      onPress={() => {
        navigation.navigate('Receive', {
          address: address,
        });
      }}>
      <WalletReceive />
      <Text style={BambooTextStyles().footerTextStyle}>
        {strings.screens.homeScreen.receive}
      </Text>
    </BambooFooterItem>
  );
}

export function SeedWarningButton({toggleModal}) {
  return (
    <BambooFooterItem
      onPress={() => {
        toggleModal();
      }}>
      <LottieView
        style={{
          width: 120,
          height: 120,
          marginBottom: -20,
          resizeMode: 'contain',
        }}
        source={require('../lottie/lf30_editor_wnyrcb26.json')}
        autoPlay
        loop
        resizeMode="cover"
      />
      <Text style={BambooTextStyles().footerTextStyle}>
        {strings.screens.seedScreen.warningButton}
      </Text>
    </BambooFooterItem>
  );
}

export function CopyAddressButton({toggleModal}) {
  return (
    <BambooFooterItem
      onPress={() => {
        toggleModal();
      }}>
      <Copy />
      <Text style={BambooTextStyles().footerTextStyle}>
        {strings.screens.receiveScreen.copyButton}
      </Text>
    </BambooFooterItem>
  );
}

export function ShowQrButton({toggleModal}) {
  return (
    <BambooFooterItem
      onPress={() => {
        toggleModal();
      }}>
      <BigQrCode />
      <Text style={BambooTextStyles().footerTextStyle}>
        {strings.screens.receiveScreen.showQr}
      </Text>
    </BambooFooterItem>
  );
}
