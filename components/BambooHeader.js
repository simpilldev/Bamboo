import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  SafeAreaView,
  initialWindowMetrics,
} from 'react-native-safe-area-context';
import * as React from 'react';
import {Hamburger, Question} from './BambooSvgs';
import {NavigationScreenProps} from 'react-navigation';
import {CustomHeader} from '../components/Header';
import { BambooTextStyles } from '../styles/textStyles';

const BambooHeader = ({navigation, title}) => {
  if (typeof title !== 'undefined' && typeof title === 'string') {
    return (
      <View style={styles.headerStyle}>
        <Text style={BambooTextStyles().titleTextStyle}>{title}</Text>
      </View>
    );
  } else {
    return (
      <View style={styles.headerStyle}>
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Hamburger width={40} height={40} style={styles.headerItemStyle} />
        </TouchableOpacity>
        <TouchableOpacity>
          <Question width={40} height={40} style={styles.headerItemStyle} />
        </TouchableOpacity>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  headerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 32,
    paddingTop:
      initialWindowMetrics.insets.top === 0
        ? 48
        : initialWindowMetrics.insets.top + 20,
    alignItems: 'center',
  },

  headerItemStyle: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
});

export default BambooHeader;
