export async function getPndPrice(ticker) {
  let response = await fetch(
    `https://api.coingecko.com/api/v3/simple/price?ids=pandacoin&vs_currencies=${ticker}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    },
  ).catch(error => {
    console.error(error);
  });
  let json = await response.json();
  let price = JSON.stringify(json.pandacoin[ticker.toLowerCase()]);
  return price;
}
