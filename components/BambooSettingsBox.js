import {View, StyleSheet, Text} from 'react-native';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Cog, Info, Padlock} from './BambooSvgs';
import {ThemeContext} from '../contexts/ThemeStore.js';
import {themes} from '../styles/colors.js';

const strings = new LocalizedStrings(require('../locale.json'));

export default function BambooSettingsBox({title, children, onPress}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <View
      style={{
        marginTop: 16,
        borderRadius: 10,
      }}>
      <TouchableOpacity
        style={{
          height: 80,
          backgroundColor: colors.boxColor,
          borderRadius: 10,
          flexDirection: 'row',
          shadowColor: colors.shadowColor,
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.17,
          shadowRadius: 2,
          elevation: 2.5,
        }}
        onPress={onPress}>
        <View
          style={{
            height: '100%',
            width: '27.5%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {children}
        </View>
        <View
          style={{
            width: '65%',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 24,
              fontFamily: 'Montserrat-Regular',
            }}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export function General() {
  return (
    <BambooSettingsBox title={strings.screens.settingsScreen.general}>
      <Cog />
    </BambooSettingsBox>
  );
}

export function Security({navigation}) {
  return (
    <BambooSettingsBox
      title={strings.screens.settingsScreen.security}
      onPress={() => {
        navigation.navigate('Security');
      }}>
      <Padlock />
    </BambooSettingsBox>
  );
}

export function About() {
  return (
    <BambooSettingsBox title={strings.screens.settingsScreen.about}>
      <Info />
    </BambooSettingsBox>
  );
}
