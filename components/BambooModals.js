import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Platform,
  Animated,
} from 'react-native';
import {themes} from '../styles/colors';
import Modal from 'react-native-modal';
import * as React from 'react';
import {BambooTextStyles} from '../styles/textStyles';
import LottieView from 'lottie-react-native';
import LocalizedStrings from 'react-native-localization';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import QRCode from 'react-native-qrcode-svg';
import ContactsDatabaseHelper from '../class/bamboo-contacts-storage';
import {DataProvider, LayoutProvider, RecyclerListView} from 'recyclerlistview';
import {Coins, Eye, Trophy} from './BambooSvgs';
import {initialWindowMetrics} from 'react-native-safe-area-context';
import SystemNavigationBar from 'react-native-system-navigation-bar';
import {Swipeable} from 'react-native-gesture-handler';
import {ThemeContext} from '../contexts/ThemeStore';
import {LoadBambooWallet} from '../class/wallets/BambooWallet';
import {BlueStorageContext} from '../blue_modules/storage-context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AppStorage, HDSegwitBech32Wallet, WatchOnlyWallet} from '../class';
import OnAppLaunch from '../class/on-app-launch';
import {getPndPrice} from './CoinGeckoHelper';
import NetworkTransactionFees from '../models/networkTransactionFees';
import {AbstractHDElectrumWallet} from '../class/wallets/abstract-hd-electrum-wallet';

const BlueElectrum = require('../../blue_modules/BlueElectrum');

const strings = new LocalizedStrings(require('../locale.json'));

function BambooModal({
  children,
  headerColor,
  headerText,
  isVisible,
  toggleModal,
  dualButtons,

  buttonColor,
  positiveButtonColor,
  negativeButtonColor,

  buttonText,
  positiveButtonText,
  negativeButtonText,

  onPress,
  onPositivePress,
  onNegativePress,

  ...props
}) {
  const deviceWidth = Dimensions.get('window').width;
  const deviceHeight =
    Platform.OS === 'ios'
      ? Dimensions.get('window').height
      : require('react-native-extra-dimensions-android').get(
          'REAL_WINDOW_HEIGHT',
        );

  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const Buttons = () => {
    const Button = ({btnColor, btnText, onPress}) => {
      return (
        <TouchableOpacity
          style={{
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 14,
            paddingHorizontal: 28,
            borderRadius: 10,
            elevation: 2,
            backgroundColor: btnColor,
          }}
          onPress={onPress}
          activeOpacity={0.5}>
          <Text
            style={{
              fontFamily: 'Montserrat-Regular',
              fontSize: 16,
              letterSpacing: 0.1,
              color: 'white',
            }}>
            {btnText}
          </Text>
        </TouchableOpacity>
      );
    };

    if (dualButtons) {
      return (
        <View
          style={{
            alignItems: 'center',
            marginBottom: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingHorizontal: 32,
            backgroundColor: colors.modalBackground,
            paddingBottom: 20,
            borderBottomRightRadius: 10,
            borderBottomLeftRadius: 10,
          }}>
          <Button
            btnColor={negativeButtonColor}
            btnText={negativeButtonText}
            onPress={() => {
              onNegativePress();
            }}
          />
          <Button
            btnColor={positiveButtonColor}
            btnText={positiveButtonText}
            onPress={() => {
              onPositivePress();
            }}
          />
        </View>
      );
    } else {
      return (
        <View
          style={{
            alignItems: 'center',
            marginBottom: 20,
            flexDirection: 'row',
            justifyContent: 'center',
            paddingHorizontal: 32,
            backgroundColor: colors.modalBackground,
            paddingBottom: 20,
            borderBottomRightRadius: 10,
            borderBottomLeftRadius: 10,
          }}>
          <Button
            btnColor={buttonColor}
            btnText={buttonText}
            onPress={() => {
              onPress();
            }}
          />
        </View>
      );
    }
  };

  const Header = ({headerColor, headerText}) => {
    return (
      <View>
        <View
          style={{
            width: '100%',
            backgroundColor: headerColor,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
            flexDirection: 'row',
            alignItems: 'center',
            alignContent: 'center',
            justifyContent: 'space-between',
          }}>
          <Text style={BambooTextStyles().modalTextTitleStyle}>
            {headerText}
          </Text>
        </View>
      </View>
    );
  };

  return (
    <Modal
      isVisible={isVisible}
      animationIn="zoomIn"
      animationOut="zoomOut"
      hardwareAccelerated
      useNativeDriver={false}
      useNativeDriverForBackdrop
      hideModalContentWhileAnimating
      onBackButtonPress={toggleModal}
      onBackdropPress={toggleModal}
      statusBarTranslucent
      deviceHeight={deviceHeight}
      deviceWidth={deviceWidth}
      {...props}
      style={{
        width: '80%',
        alignSelf: 'center',
      }}>
      <Header headerText={headerText} headerColor={headerColor} />
      {children}
      <Buttons
        dualButtons={dualButtons}
        onPress={onPress}
        buttonText={buttonText}
        buttonColor={buttonColor}
      />
    </Modal>
  );
}

export function BambooLoadingModal({toggleModal, isVisible, onLayout, text}) {
  const deviceWidth = Dimensions.get('window').width;
  const deviceHeight =
    Platform.OS === 'ios'
      ? Dimensions.get('window').height
      : require('react-native-extra-dimensions-android').get(
          'REAL_WINDOW_HEIGHT',
        );

  const topInset = initialWindowMetrics.insets.top;
  const bottomInset = initialWindowMetrics.insets.bottom;

  const {loadingMsg, setLoadingMsg} = React.useState('');

  return (
    <Modal
      isVisible={isVisible}
      useNativeDriverForBackdrop
      hasBackdrop
      coverScreen
      useNativeDriver
      hideModalContentWhileAnimating
      hardwareAccelerated
      deviceHeight={deviceHeight}
      deviceWidth={deviceWidth}
      backdropOpacity={0.95}
      statusBarTranslucent
      style={{
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
      }}>
      <LottieView
        source={require('../../lottie/lottie_splash_wallet_loading.json')}
        style={{
          height: 200,
          width: 200,
        }}
        hardwareAccelerationAndroid
        cacheStrategy="strong"
        renderMode="HARDWARE"
        autoPlay
        onLayout={onLayout}
      />
      <Text style={[BambooTextStyles().regularTextStyle, {marginTop: 20}]}>
        {text}
      </Text>
    </Modal>
  );
}

export function GenerateWalletModal({toggleModal, isVisible, label, goToHome}) {
  const {addAndSaveWallet, setSelectedWallet} =
    React.useContext(BlueStorageContext);

  console.log(typeof label);
  console.log('Label = ' + label);

  const generateWallet = async () => {
    const wallet = new HDSegwitBech32Wallet();
    await wallet.generate();

    console.log('Secret = ' + wallet.getSecret());
    wallet.validateMnemonic()
      ? console.log('Valid')
      : console.error('INVALID MNEMONIC');
    wallet.setLabel(label);

    await addAndSaveWallet(wallet);
    await wallet.fetchBalance();
    await wallet.fetchTransactions();

    console.log('ID = ' + wallet.getID());

    setSelectedWallet(wallet.getID());
    await OnAppLaunch.setSelectedDefaultWallet(wallet.getID());
    goToHome();
  };
  return (
    <BambooLoadingModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      onLayout={generateWallet}
      text={strings.modals.generateWallet.title}
    />
  );
}

export function CreatingTxModal({toggleModal, isVisible, address, amount}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;
  const {wallets, selectedWallet} = React.useContext(BlueStorageContext);
  const wallet = wallets.find(w => selectedWallet === w.getID());

  React.useEffect(() => {
    const getChangeAddressAsync = async wallet => {
      if (changeAddress) return changeAddress; // cache

      let change;
      if (WatchOnlyWallet.type === wallet.type && !wallet.isHd()) {
        // plain watchonly - just get the address
        change = wallet.getAddress();
      } else {
        // otherwise, lets call widely-used getChangeAddressAsync()
        try {
          change = await Promise.race([
            sleep(2000),
            wallet.getChangeAddressAsync(),
          ]);
        } catch (_) {}

        if (!change) {
          // either sleep expired or getChangeAddressAsync threw an exception
          if (wallet instanceof AbstractHDElectrumWallet) {
            change = wallet._getInternalAddressByIndex(
              wallet.getNextFreeChangeAddressIndex(),
            );
          } else {
            // legacy wallets
            change = wallet.getAddress();
          }
        }
      }

      if (change) setChangeAddress(change); // cache

      return change;
    };
    getChangeAddressAsync(wallet);
  }, [wallet]);

  const [networkTransactionFees, setNetworkTransactionFees] = React.useState(
    new NetworkTransactionFees(3, 2, 1),
  );
  const [feePrecalc, setFeePrecalc] = React.useState({
    current: null,
    slowFee: null,
    mediumFee: null,
    fastestFee: null,
  });
  const feeRate = React.useMemo(() => {
    if (feePrecalc.slowFee === null) return '1'; // wait for precalculated fees
    let initialFee;
    if (feePrecalc.fastestFee !== null) {
      initialFee = String(networkTransactionFees.fastestFee);
    } else if (feePrecalc.mediumFee !== null) {
      initialFee = String(networkTransactionFees.mediumFee);
    } else {
      initialFee = String(networkTransactionFees.slowFee);
    }
    return initialFee;
  }, [feePrecalc, networkTransactionFees]);
  const [changeAddress, setChangeAddress] = React.useState();

  const requestedSatPerByte = feeRate;

  const createTx = async () => {
    await BlueElectrum.ping();
    await BlueElectrum.waitTillConnected();
    await wallet.fetchUtxo();
    console.log(address);
    console.log(amount);
    const targets = [];
    targets.push({address: address, value: amount});
    const requestedSatPerByte = Number(feeRate);
    console.log(wallet.getLabel());
    console.warn('Change address = ' + changeAddress);
    const utxo = wallet.getUtxo();
    console.log('UTXO length = ' + utxo.length);
    console.log('UTXO txid = ' + utxo[0].txid);
    console.log('UTXO vout = ' + utxo[0].vout);
    console.log('UTXO value = ' + utxo[0].value);
    const {tx} = wallet.createTransaction(
      utxo,
      targets,
      requestedSatPerByte,
      changeAddress,
      HDSegwitBech32Wallet.defaultRBFSequence,
    );

    console.log('Tx = ' + tx);

    // const broadcast = async tx => {
    //   await BlueElectrum.ping();
    //   await BlueElectrum.waitTillConnected();

    //   const result = await wallet.broadcastTx(tx);
    //   if (!result) {
    //     throw new Error('Error broadcasting transaction :(');
    //   }

    //   return result;
    // };
    toggleModal();
  };

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons
      headerColor={colors.modalGreen}
      headerText={strings.modals.sendConfirmation.title}
      positiveButtonColor={colors.modalGreen}
      negativeButtonColor={colors.modalRed}
      positiveButtonText={strings.modals.sendConfirmation.posButton}
      negativeButtonText={strings.modals.sendConfirmation.negButton}
      onPositivePress={async () => {
        await createTx();
        toggleModal();
      }}
      onNegativePress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.sendConfirmation.body
          .replace('{0}', amount)
          .replace('{1}', address)}
      </Text>
    </BambooModal>
  );
}

export function AddingWalletModal({toggleModal, isVisible, label}) {
  const {addAndSaveWallet, setSelectedWallet} =
    React.useContext(BlueStorageContext);

  console.log(typeof label);
  console.log('Label = ' + label);

  const generateWallet = async () => {
    const wallet = new HDSegwitBech32Wallet();
    await wallet.generate();

    console.log('Secret = ' + wallet.getSecret());
    wallet.validateMnemonic()
      ? console.log('Valid')
      : console.error('INVALID MNEMONIC');
    wallet.setLabel(label);

    await addAndSaveWallet(wallet);
    await wallet.fetchBalance();
    await wallet.fetchTransactions();

    console.log('ID = ' + wallet.getID());

    toggleModal();
  };
  return (
    <BambooLoadingModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      onLayout={generateWallet}
      text={strings.modals.generateWallet.title}
    />
  );
}

export function BambooToastModal({toggleModal, isVisible, isTop, children}) {
  const deviceWidth = Dimensions.get('window').width;
  const deviceHeight =
    Platform.OS === 'ios'
      ? Dimensions.get('window').height
      : require('react-native-extra-dimensions-android').get(
          'REAL_WINDOW_HEIGHT',
        );

  const topInset = initialWindowMetrics.insets.top;
  const bottomInset = initialWindowMetrics.insets.bottom;

  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <Modal
      isVisible={isVisible}
      animationIn="fadeInDownBig"
      animationOut="fadeOut"
      useNativeDriverForBackdrop
      onBackButtonPress={toggleModal}
      onBackdropPress={toggleModal}
      hideModalContentWhileAnimating
      onSwipeComplete={() => toggleModal()}
      swipeDirection={['left', 'right', isTop ? 'up' : 'down']}
      swipeThreshold={50}
      hasBackdrop
      backdropOpacity={0}
      statusBarTranslucent
      deviceHeight={deviceHeight}
      deviceWidth={deviceWidth}
      style={{
        marginTop: isTop ? (topInset === 0 ? 32 : topInset) : 0,
        marginBottom: !isTop ? (bottomInset === 0 ? 32 : bottomInset) : 0,
        justifyContent: isTop ? 'flex-start' : 'flex-end',
      }}>
      <View
        style={{
          backgroundColor: colors.toastModalBackground,
          width: '100%',
          minHeight: 80,
          flexDirection: 'row',
          alignSelf: 'center',
          borderRadius: 20,
        }}>
        <TouchableOpacity
          style={{
            width: '22.5%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Trophy />
        </TouchableOpacity>

        <View
          style={{
            paddingVertical: 12,
            width: '77.5%',
          }}>
          <Text
            style={[
              BambooTextStyles().toastModalTextTitleStyle,
              {
                paddingBottom: 8,
                paddingRight: 16,
                textAlignVertical: 'bottom',
              },
            ]}>
            Gotta protect 'em all!
          </Text>
          <Text
            style={[
              BambooTextStyles().toastModalTextBodyStyle,
              {
                textAlignVertical: 'top',
                paddingRight: 24,
              },
            ]}>
            Send some Pandacoin to the WWF donation address.
          </Text>
        </View>
      </View>
    </Modal>
  );
}

export function PasswordModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      dualButtons
      headerColor={colors.modalHeaderDefault}
      headerText="Password"
      positiveButtonText="Done"
      negativeButtonText="Cancel"
      onPositivePress={() => {
        toggleModal();
      }}
      onNegativePress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        Please enter your password below to unlock your wallet.
      </Text>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          marginHorizontal: 32,
          paddingBottom: 120,
        }}>
        <Text style={BambooTextStyles().regularTextStyle}>
          Please choose a password for your Pandacoin wallet.
        </Text>
        <View
          style={{
            width: '100%',
            height: 80,
            borderRadius: 20,
            backgroundColor: colors.wildWillow,
            borderColor: '#fff',
            borderWidth: 1.5,
          }}>
          <TextInput
            style={BambooTextStyles().regularTextStyle}
            placeholder="Enter your password"
            placeholderTextColor="white"
            autoCapitalize="none"
            keyboardType="email-address"
            returnKeyType="next"
            underlineColorAndroid="#f000"
            blurOnSubmit={false}
          />
        </View>
        <View
          style={[
            {
              width: '100%',
              height: 80,
              borderRadius: 20,
              backgroundColor: colors.wildWillow,
              borderColor: '#fff',
              borderWidth: 1.5,
            },
            {marginTop: 16},
          ]}>
          <TextInput
            style={BambooTextStyles().regularTextStyle}
            placeholder="Re-enter your password"
            placeholderTextColor="white"
            autoCapitalize="none"
            keyboardType="email-address"
            returnKeyType="next"
            underlineColorAndroid="#f000"
            blurOnSubmit={false}
          />
        </View>
      </View>
    </BambooModal>
  );
}

export function NoInternetModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      dualButtons
      headerText="No Internet"
      headerColor={colors.modalRed}
      positiveButtonText="Ok"
      negativeButtonText="More Info"
      negativeButtonColor={colors.modalRed}
      positiveButtonColor={colors.modalGreen}
      onPositivePress={() => {
        toggleModal();
      }}
      onNegativePress={() => {
        toggleModal();
      }}>
      <Text style={BambooTextStyles().modalBodyTextStyleNoBottomPadding}>
        Bamboo wallet was unable to connect to the internet :(
      </Text>
      <View
        style={{
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <LottieView
          style={{
            width: 180,
            height: 180,
            resizeMode: 'contain',
            backgroundColor: 'white',
          }}
          source={require('../lottie/11645-no-internet-animation.json')}
          autoPlay
          loop
          resizeMode="cover"
        />
      </View>
    </BambooModal>
  );
}

export function SeedPhraseWarningModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      headerColor={colors.modalRed}
      headerText="Warning"
      dualButtons
      negativeButtonColor={colors.modalRed}
      positiveButtonColor={colors.modalGreen}
      negativeButtonText={strings.modals.seedPhrase.negButton}
      positiveButtonText={strings.modals.seedPhrase.posButton}
      onNegativePress={() => toggleModal()}
      onPositivePress={() => toggleModal()}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.seedPhrase.body}
      </Text>
    </BambooModal>
  );
}

export function QrCodeModal({toggleModal, isVisible, address}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      headerText={strings.modals.qrCode.title}
      headerColor={colors.modalHeaderDefault}
      dualButtons={false}
      buttonText={strings.modals.qrCode.button}
      buttonColor={colors.modalGreen}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.qrCode.body.replace('{0}', address)}
      </Text>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: colors.modalBackground,
          paddingBottom: 32,
        }}>
        <QRCode
          size={210}
          value={address}
          ecl="H"
          color={colors.qrCodeColor}
          backgroundColor={colors.modalBackground}
        />
      </View>
    </BambooModal>
  );
}

export function AddressCopiedModal({toggleModal, isVisible, address}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      headerText={strings.modals.addressCopied.title}
      headerColor={colors.modalGreen}
      dualButtons={false}
      buttonText={strings.modals.addressCopied.button}
      buttonColor={colors.modalGreen}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.addressCopied.body.replace('{0}', address)}
      </Text>
    </BambooModal>
  );
}

export function InvalidAddressModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      headerColor={colors.modalRed}
      headerText={strings.modals.invalidAddress.title}
      dualButtons={false}
      buttonText={strings.modals.invalidAddress.button}
      buttonColor={colors.modalRed}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.invalidAddress.body}
      </Text>
    </BambooModal>
  );
}

export function InvalidAmountModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      headerColor={colors.modalRed}
      headerText={strings.modals.invalidAmount.title}
      dualButtons={false}
      buttonText={strings.modals.invalidAmount.button}
      buttonColor={colors.modalRed}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.invalidAmount.body}
      </Text>
    </BambooModal>
  );
}

export function InvalidLabelModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      headerColor={colors.modalRed}
      headerText={strings.modals.invalidLabel.title}
      dualButtons={false}
      buttonText={strings.modals.invalidLabel.button}
      buttonColor={colors.modalRed}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.invalidLabel.body}
      </Text>
    </BambooModal>
  );
}

export function NewContactModal({
  toggleModal,
  isVisible,
  toggleUpdate,
  toggleErrorModal,
  toggleAddressExistsModal,
  fetchAddresses,
}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const db = new ContactsDatabaseHelper();
  const [address, setAddress] = React.useState('');
  const [label, setLabel] = React.useState('');

  const [addresses, setAddresses] = React.useState([]);
  React.useEffect(() => {
    setAddresses(fetchAddresses());
  }, [toggleUpdate]);

  const checkIfAddressExists = address => {
    console.log('Checking if ' + address + ' exists.');
    let bool = false;
    addresses.forEach(element => {
      console.log('Element = ' + element);

      if (address === element) {
        console.warn('Address Exists');
        bool = true;
      }
    });
    return bool;
  };

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons
      headerColor={colors.modalHeaderDefault}
      headerText={strings.modals.createLabel.title}
      positiveButtonColor={colors.modalGreen}
      negativeButtonColor={colors.modalRed}
      positiveButtonText={strings.modals.createLabel.posButton}
      negativeButtonText={strings.modals.createLabel.negButton}
      onPositivePress={() => {
        if (checkIfAddressExists(address)) {
          toggleAddressExistsModal();
          console.warn('Address exists');
        } else {
          if (db.insertNewContact(label, address)) {
            toggleUpdate();
            toggleModal();
          } else {
            toggleErrorModal();
          }
        }
      }}
      onNegativePress={() => {
        toggleModal();
      }}
      onModalShow={() => {
        addresses.forEach(element => {
          console.log('Element = ' + element);
        });
      }}>
      <View
        style={{
          paddingTop: 32,
          paddingBottom: 48,
          paddingHorizontal: 32,
          backgroundColor: colors.modalBackground,
        }}>
        <View
          style={{
            width: '100%',
          }}>
          <Text
            style={{
              fontSize: 18,
              color: colors.modalTextColor,
              textAlign: 'left',
              fontFamily: 'Montserrat-Light',
            }}>
            {strings.modals.createLabel.name}
          </Text>
          <TextInput
            style={{
              backgroundColor: colors.modalBoxColor,
              marginTop: 16,
              paddingLeft: 16,
              width: '100%',
              justifyContent: 'space-around',
              color: colors.modalTextColor,
              borderRadius: 10,
              borderColor: colors.borderColor,
              borderWidth: 1,
              flexDirection: 'row',
              alignItems: 'center',
            }}
            keyboardType="default"
            blurOnSubmit={false}
            underlineColorAndroid="#f000"
            returnKeyType="done"
            onChangeText={value => setLabel(value)}
          />
        </View>
        <View
          style={{
            marginTop: 32,
            width: '100%',
          }}>
          <Text
            style={{
              fontSize: 18,
              color: colors.modalTextColor,
              textAlign: 'left',
              fontFamily: 'Montserrat-Light',
            }}>
            {strings.modals.createLabel.address}
          </Text>
          <TextInput
            style={{
              backgroundColor: colors.modalBoxColor,
              marginTop: 16,
              paddingLeft: 16,
              width: '100%',
              justifyContent: 'space-around',
              color: colors.modalTextColor,
              borderRadius: 10,
              borderColor: colors.borderColor,
              borderWidth: 1,
              flexDirection: 'row',
              alignItems: 'center',
            }}
            keyboardType="default"
            blurOnSubmit={false}
            underlineColorAndroid="#f000"
            returnKeyType="done"
            onChangeText={value => setAddress(value)}
          />
        </View>
      </View>
    </BambooModal>
  );
}

export function AddressesExistsModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      headerColor={colors.modalRed}
      headerText={strings.modals.addressExists.title}
      dualButtons={false}
      buttonText={strings.modals.addressExists.button}
      buttonColor={colors.modalRed}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.addressExists.body}
      </Text>
    </BambooModal>
  );
}

export function SendConfirmationModal({
  toggleModal,
  isVisible,
  amount,
  address,
}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons
      headerColor={colors.modalGreen}
      headerText={strings.modals.sendConfirmation.title}
      positiveButtonColor={colors.modalGreen}
      negativeButtonColor={colors.modalRed}
      positiveButtonText={strings.modals.sendConfirmation.posButton}
      negativeButtonText={strings.modals.sendConfirmation.negButton}
      onPositivePress={() => {
        toggleModal();
      }}
      onNegativePress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.sendConfirmation.body
          .replace('{0}', amount)
          .replace('{1}', address)}
      </Text>
    </BambooModal>
  );
}

export function MissingEntryModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      toggleModal={toggleModal}
      isVisible={isVisible}
      headerColor={colors.modalRed}
      headerText={strings.modals.missingEntry.title}
      dualButtons={false}
      buttonText={strings.modals.missingEntry.button}
      buttonColor={colors.modalRed}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.missingEntry.body}
      </Text>
    </BambooModal>
  );
}

export function PasswordsDoNotMatchModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons={false}
      headerColor={colors.modalRed}
      headerText={strings.modals.passwordsDoNotMatch.title}
      buttonColor={colors.modalRed}
      buttonText={strings.modals.passwordsDoNotMatch.button}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.passwordsDoNotMatch.body}
      </Text>
    </BambooModal>
  );
}

export function WeakPasswordModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons={false}
      headerColor={colors.modalRed}
      headerText={strings.modals.weakPassword.title}
      buttonColor={colors.modalRed}
      buttonText={strings.modals.passwordsDoNotMatch.button}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.weakPassword.body}
      </Text>
    </BambooModal>
  );
}

export function EmptyPasswordModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons={false}
      headerColor={colors.modalRed}
      headerText={strings.modals.emptyPassword.title}
      buttonColor={colors.modalRed}
      buttonText={strings.modals.passwordsDoNotMatch.button}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.emptyPassword.body}
      </Text>
    </BambooModal>
  );
}

export function ImportFailedModal({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons={false}
      headerColor={colors.modalRed}
      headerText={strings.modals.importFailed.title}
      buttonColor={colors.modalRed}
      buttonText={strings.modals.importFailed.button}
      onPress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.importFailed.body}
      </Text>
    </BambooModal>
  );
}

export function SelectCurrencyModal({
  toggleModal,
  isVisible,
  setCurrency,
  convertPndToOtherCurrency,
}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const [currencies, setCurrencies] = React.useState(
    require('../models/currencies.json'),
  );
  const [loading, setLoading] = React.useState(true);

  const [dataProvider, setDataProvider] = React.useState(
    new DataProvider((r1, r2) => {
      return r1 !== r2;
    }).cloneWithRows(currencies),
  );

  React.useEffect(() => {
    setCurrencies(require('../models/currencies.json'));
    setDataProvider(
      new DataProvider((r1, r2) => {
        return r1 !== r2;
      }).cloneWithRows(currencies),
    );
    setLoading(false);
  }, [loading]);

  const {width} = Dimensions.get('window');

  const layoutProvider = new LayoutProvider(
    index => {
      return 0;
    },
    (type, dim) => {
      dim.width = width;
      dim.height = 54;
    },
  );

  const rowRenderer = (type, data, index) => {
    const row = [];
    let prevOpenedRow;

    const closeRow = index => {
      console.log('closerow');
      if (prevOpenedRow && prevOpenedRow !== row[index]) {
        prevOpenedRow.close();
      }
      prevOpenedRow = row[index];
    };

    return (
      <TouchableOpacity
        style={{
          paddingVertical: 8,
          borderTopWidth: 0.75,
          borderBottomWidth: 0.75,
          borderTopColor: colors.borderColor,
          borderBottomColor: colors.borderColor,
        }}
        onPress={async () => {
          setCurrency(currencies[index]);
          let fetchedPrice = await getPndPrice(currencies[index]);
          convertPndToOtherCurrency(fetchedPrice);
          toggleModal();
        }}>
        <Text
          style={{
            fontFamily: 'Montserrat-Regular',
            fontSize: 16,
            color: colors.modalTextColor,
            backgroundColor: colors.modalBackground,
            paddingHorizontal: 32,
          }}>
          {currencies[index].toUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  };

  const getRecyclerView = () => {
    if (loading || dataProvider.getSize() === 0) {
      return null;
    } else {
      return (
        <RecyclerListView
          dataProvider={dataProvider}
          layoutProvider={layoutProvider}
          rowRenderer={rowRenderer}
        />
      );
    }
  };

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons={false}
      headerColor={colors.modalHeaderDefault}
      headerText={strings.modals.selectCurrency.title}
      buttonColor={colors.modalGreen}
      buttonText={strings.modals.selectCurrency.button}
      onPress={() => {
        toggleModal();
      }}>
      <View
        style={{
          backgroundColor: colors.modalBackground,
          flex: 1,
          paddingVertical: 32,
          maxHeight: '50%',
        }}>
        {getRecyclerView()}
      </View>
    </BambooModal>
  );
}

export function WhatIsPandacoin({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons
      headerColor={colors.modalHeaderDefault}
      headerText={strings.modals.whatIsPandacoin.title}
      positiveButtonColor={colors.modalGreen}
      negativeButtonColor={colors.modalRed}
      positiveButtonText={strings.modals.whatIsPandacoin.posButton}
      negativeButtonText={strings.modals.whatIsPandacoin.negButton}
      onPositivePress={() => {
        toggleModal();
      }}
      onNegativePress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}>
        {strings.modals.whatIsPandacoin.body}
      </Text>
    </BambooModal>
  );
}

export function Help({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons
      headerColor={colors.modalHeaderDefault}
      headerText={strings.modals.help.title}
      positiveButtonColor={colors.modalGreen}
      negativeButtonColor={colors.modalRed}
      positiveButtonText={strings.modals.help.posButton}
      negativeButtonText={strings.modals.help.negButton}
      onPositivePress={() => {
        toggleModal();
      }}
      onNegativePress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}
        dataDetectorType="all">
        {strings.modals.help.body}
      </Text>
    </BambooModal>
  );
}

export function HelpImport({toggleModal, isVisible}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons
      headerColor={colors.modalHeaderDefault}
      headerText={strings.modals.importHelp.title}
      positiveButtonColor={colors.modalGreen}
      negativeButtonColor={colors.modalRed}
      positiveButtonText={strings.modals.importHelp.posButton}
      negativeButtonText={strings.modals.importHelp.negButton}
      onPositivePress={() => {
        toggleModal();
      }}
      onNegativePress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}
        dataDetectorType="all">
        {strings.modals.importHelp.body}
      </Text>
    </BambooModal>
  );
}

export function WalletLabel({
  toggleModal,
  isVisible,
  onFinish,
  label,
  setLabel,
}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const [borderColor, setBorderColor] = React.useState(colors.borderColor);

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons
      headerColor={colors.modalHeaderDefault}
      headerText={strings.modals.walletLabel.title}
      positiveButtonColor={colors.modalGreen}
      negativeButtonColor={colors.modalRed}
      positiveButtonText={strings.modals.walletLabel.posButton}
      negativeButtonText={strings.modals.walletLabel.negButton}
      onPositivePress={() => {
        toggleModal();
        onFinish();
      }}
      onNegativePress={() => {
        toggleModal();
      }}>
      <View
        style={{
          paddingTop: 64,
          paddingBottom: 64,
          paddingHorizontal: 32,
          backgroundColor: colors.modalBackground,
        }}>
        <View
          style={{
            width: '100%',
          }}>
          <Text
            style={{
              fontSize: 18,
              color: colors.modalTextColor,
              textAlign: 'left',
              fontFamily: 'Montserrat-Light',
            }}>
            {strings.modals.walletLabel.createLabel}
          </Text>
          <TextInput
            style={{
              backgroundColor: colors.modalBoxColor,
              marginTop: 16,
              paddingLeft: 16,
              width: '100%',
              justifyContent: 'space-around',
              color: colors.modalTextColor,
              borderRadius: 10,
              borderColor: borderColor,
              borderWidth: 1,
              flexDirection: 'row',
              alignItems: 'center',
            }}
            keyboardType="default"
            blurOnSubmit={false}
            underlineColorAndroid="#f000"
            returnKeyType="done"
            onChangeText={value => {
              setLabel(value);
              if (label.length > 0) setBorderColor(colors.inputCorrect);
            }}
          />
        </View>
      </View>
    </BambooModal>
  );
}

export function EncryptModal({
  toggleModal,
  isVisible,
  setEncryptionSwitchEnabled,
}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const {isStorageEncrypted, encryptStorage, decryptStorage, saveToDisk} =
    React.useContext(BlueStorageContext);

  const [passwordsVisible, setPasswordsVisible] = React.useState(false);
  const togglePasswordsVisible = () => {
    setPasswordsVisible(!passwordsVisible);
  };

  const [badPasswordModalVisible, setBadPasswordModalVisible] =
    React.useState(false);
  const toggleBadPasswordModal = () => {
    setBadPasswordModalVisible(!badPasswordModalVisible);
  };

  const [passwordsDoNotMatchModalVisible, setPasswordsDoNotMatchModalVisible] =
    React.useState(false);
  const togglePasswordsDoNotMatchModal = () => {
    setPasswordsDoNotMatchModalVisible(!passwordsDoNotMatchModalVisible);
  };

  const [emptyPasswordModalVisible, setEmptyPasswordModalVisible] =
    React.useState(false);
  const toggleEmptyPasswordModal = () => {
    setEmptyPasswordModalVisible(!emptyPasswordModalVisible);
  };

  const [password, setPassword] = React.useState('');
  const [password2, setPassword2] = React.useState('');

  const passwordsMatch = () => {
    if (password.length <= 0) {
      toggleEmptyPasswordModal();
      return false;
    } else if (password === password2) {
      return true;
    } else {
      togglePasswordsDoNotMatchModal();
      return false;
    }
  };

  const isStrong = () => {
    console.log('Password length = ' + password.length);
    if (password.length >= 6) {
      return true;
    } else {
      toggleBadPasswordModal();
      return false;
    }
  };

  const encrypt = async password => {
    await encryptStorage(password);
    await saveToDisk();
    return await isStorageEncrypted();
  };

  return (
    <BambooModal
      isVisible={isVisible}
      toggleModal={toggleModal}
      dualButtons
      headerColor={colors.modalHeaderDefault}
      headerText={strings.modals.encryptWallet.title}
      positiveButtonColor={colors.modalGreen}
      negativeButtonColor={colors.modalRed}
      positiveButtonText={strings.modals.encryptWallet.posButton}
      negativeButtonText={strings.modals.encryptWallet.negButton}
      onPositivePress={async () => {
        if (passwordsMatch() && isStrong()) {
          const enc = await encrypt(password);
          console.log('Was enc successful = ' + enc);
          setEncryptionSwitchEnabled(enc);
          toggleModal();
        }
      }}
      onNegativePress={() => {
        toggleModal();
      }}>
      <Text
        style={[
          BambooTextStyles().regularTextStyle,
          BambooTextStyles().modalBodyTextStyle,
        ]}
        dataDetectorType="all">
        {strings.modals.encryptWallet.body}
      </Text>
      <View
        style={{
          backgroundColor: colors.modalBackground,
        }}>
        <View
          style={{
            marginHorizontal: 32,
            marginVertical: 16,
            justifyContent: 'space-around',
            borderRadius: 10,
            backgroundColor: colors.boxColor,
            height: 80,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{flex: 0.75, flexDirection: 'column'}}>
            <TextInput
              style={[
                BambooTextStyles().smallTextStyle,
                {flex: 1, paddingStart: 24, textAlignVertical: 'center'},
              ]}
              placeholder={strings.screens.createPassScreen.enterPassword}
              placeholderTextColor="white"
              value={password}
              onChangeText={password => setPassword(password)}
              secureTextEntry={passwordsVisible}
              multiline={false}
              autoCapitalize="none"
              autoCorrect={false}
              textContentType="newPassword"
              keyboardType="default"
              enablesReturnKeyAutomatically
              returnKeyType="next"
              underlineColorAndroid="#f000"
              blurOnSubmit={false}
            />
          </View>
          <TouchableOpacity
            style={{
              height: '100%',
              flex: 0.25,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={togglePasswordsVisible}>
            <Eye arePasswordsVisible={passwordsVisible} />
          </TouchableOpacity>
        </View>

        <View
          style={{
            marginHorizontal: 32,
            marginTop: 24,
            marginBottom: 48,
            justifyContent: 'space-around',
            borderRadius: 10,
            backgroundColor: colors.boxColor,
            height: 80,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{flex: 0.75, flexDirection: 'column'}}>
            <TextInput
              style={[
                BambooTextStyles().smallTextStyle,
                {flex: 1, paddingStart: 24, textAlignVertical: 'center'},
              ]}
              placeholder={strings.screens.createPassScreen.reenterPassword}
              placeholderTextColor="white"
              value={password2}
              onChangeText={password2 => setPassword2(password2)}
              secureTextEntry={passwordsVisible}
              multiline={false}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="default"
              returnKeyType="next"
              underlineColorAndroid="#f000"
              blurOnSubmit={false}
            />
          </View>
          <TouchableOpacity
            style={{
              height: '100%',
              flex: 0.25,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={togglePasswordsVisible}>
            <Eye arePasswordsVisible={passwordsVisible} />
          </TouchableOpacity>
        </View>
      </View>

      <PasswordsDoNotMatchModal
        toggleModal={togglePasswordsDoNotMatchModal}
        isVisible={passwordsDoNotMatchModalVisible}
      />

      <WeakPasswordModal
        toggleModal={toggleBadPasswordModal}
        isVisible={badPasswordModalVisible}
      />

      <EmptyPasswordModal
        toggleModal={toggleEmptyPasswordModal}
        isVisible={emptyPasswordModalVisible}
      />
    </BambooModal>
  );
}
