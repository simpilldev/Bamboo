import * as React from 'react';
import {ThemeProvider} from 'styled-components';
import {ThemeContext} from '../contexts/ThemeStore';
import {themes} from '../styles/colors';

const Theme = ({children}) => {
  const {isDarkMode} = React.useContext(ThemeContext); // get the current theme ('light' or 'dark')
  return (
    <ThemeProvider theme={isDarkMode ? themes.dark : themes.light}>
      {children}
    </ThemeProvider>
  );
};

export default Theme;
