import React, {useContext, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Switch} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import colors, {themes} from '../styles/colors';
import LocalizedStrings from 'react-native-localization';
import {
  Add,
  BigQrCode,
  Coins,
  Copy,
  QrCode,
  Wallet,
  WalletReceive,
  WalletSend,
} from './BambooSvgs';
import {initialWindowMetrics} from 'react-native-safe-area-context';
import LottieView from 'lottie-react-native';
import {ThemeContext} from '../contexts/ThemeStore';
import {BambooTextStyles, textStyles} from '../styles/textStyles';
import {BlueStorageContext} from '../blue_modules/storage-context';
import {HDSegwitBech32Wallet} from '../class';
import BambooBox from './BambooBox';

const strings = new LocalizedStrings(require('../locale.json'));

export default function BambooSwitch({isEnabled, onToggle, text}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <View
      style={{
        height: 80,
        width: '100%',
        marginTop: 32,
        backgroundColor: colors.boxColor,
        borderRadius: 10,
        flexDirection: 'row',
        shadowColor: colors.shadowColor,
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.17,
        shadowRadius: 2,
        elevation: 2.5,
      }}>
      <View
        style={{
          flexDirection: 'row',
          flex: 0.7,
        }}>
        <Text
          style={[
            {
              textAlignVertical: 'center',
              marginStart: 32,
            },
            BambooTextStyles().smallTextStyle,
          ]}>
          {text}
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          flex: 0.3,
          justifyContent: 'flex-end',
        }}>
        <Switch
          trackColor={{false: '#c28770', true: '#9ad4ab'}}
          thumbColor={isEnabled ? '#70c287' : '#f4f3f4'}
          ios_backgroundColor="#3e3e3e"
          onValueChange={() => {
            onToggle();
          }}
          value={isEnabled}
          style={{
            marginEnd: 24,
          }}
        />
      </View>
    </View>
  );
}
