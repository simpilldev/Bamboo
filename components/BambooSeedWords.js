import * as React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import BambooBox from './BambooBox';
import {darkTheme, lightTheme} from '../../styles/colors';
import {ThemeContext} from '../contexts/ThemeStore.js';
import {themes} from '../styles/colors.js';
import {BlueStorageContext} from '../blue_modules/storage-context';

export function SeedWordsColumn() {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const {wallets, selectedWallet} = React.useContext(BlueStorageContext);

  const seedWordsArray = getSeedWordsArray(
    wallets.find(w => selectedWallet === w.getID()),
  );

  return (
    <View
      style={{
        flex: 1,
        marginLeft: 16,
        marginTop: 32,
        flexDirection: 'row',
        justifyContent: 'center',
        // alignSelf: 'center',
        // alignItems: 'center',
        // alignContent: 'center',
      }}>
      <View style={{width: '45%', marginLeft: 32, alignItems: 'flex-start'}}>
        <SeedWordBox seedWord={seedWordsArray[0]} seedWordNumber={1} />
        <SeedWordBox seedWord={seedWordsArray[1]} seedWordNumber={2} />
        <SeedWordBox seedWord={seedWordsArray[2]} seedWordNumber={3} />
        <SeedWordBox seedWord={seedWordsArray[3]} seedWordNumber={4} />
        <SeedWordBox seedWord={seedWordsArray[4]} seedWordNumber={5} />
        <SeedWordBox seedWord={seedWordsArray[5]} seedWordNumber={6} />
      </View>
      <View style={{width: '45%', marginRight: 32, alignItems: 'flex-start'}}>
        <SeedWordBox seedWord={seedWordsArray[6]} seedWordNumber={7} />
        <SeedWordBox seedWord={seedWordsArray[7]} seedWordNumber={8} />
        <SeedWordBox seedWord={seedWordsArray[8]} seedWordNumber={9} />
        <SeedWordBox seedWord={seedWordsArray[9]} seedWordNumber={10} />
        <SeedWordBox seedWord={seedWordsArray[10]} seedWordNumber={11} />
        <SeedWordBox seedWord={seedWordsArray[11]} seedWordNumber={12} />
      </View>
    </View>
  );
}

function getSeedWordsArray(wallet) {
  return wallet.getSecret().split(' ');
}

function SeedWordBox({seedWordNumber, seedWord}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooBox height={50} style={{marginBottom: 24}}>
      <Text
        style={{
          fontFamily: 'Montserrat-Regular',
          paddingHorizontal: 15,
          fontSize: 16,
          textAlignVertical: 'center',
          color: colors.buttonTextColor,
        }}>
        {seedWordNumber}. {seedWord}
      </Text>
    </BambooBox>
  );
}
