import React, {useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Touchable,
  TouchableWithoutFeedback,
  Animated,
  Easing,
  SafeAreaView,
} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import LocalizedStrings from 'react-native-localization';
import {Coins, Wallet} from './BambooSvgs';
import {initialWindowMetrics} from 'react-native-safe-area-context';
import LottieView from 'lottie-react-native';
import {saveTheme, ThemeContext} from '../contexts/ThemeStore';
import {themes} from '../styles/colors';
import SystemNavigationBar from 'react-native-system-navigation-bar';
import {getFormattedWalletBalance} from '../class/bamboo-currency.js';
import {Help, WhatIsPandacoin} from './BambooModals';
import {BlueStorageContext} from '../blue_modules/storage-context';

const strings = new LocalizedStrings(require('../locale.json'));

const BambooDrawer = (props, navigation) => {
  const {isDarkMode, setDarkMode} = useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const {wallets, selectedWallet} = useContext(BlueStorageContext);

  const wallet = wallets.find(w => selectedWallet === w.getID());

  const {state, ...rest} = props;
  const newState = {...state};
  newState.routes = newState.routes.filter(
    item =>
      item.name !== 'Send' &&
      item.name !== 'Receive' &&
      item.name !== 'ScanQr' &&
      item.name !== 'Security',
  );

  const animationProgress = React.useRef(
    new Animated.Value(isDarkMode ? 0.5 : 1),
  );

  const [pndBlurbModalVisible, setPndBlurbModalVisible] = React.useState(false);
  const togglePndBlurbModal = () => {
    setPndBlurbModalVisible(!pndBlurbModalVisible);
  };
  const [helpModalVisible, setHelpModalVisible] = React.useState(false);
  const toggleHelpModal = () => {
    setHelpModalVisible(!helpModalVisible);
  };

  return (
    <View
      style={{flex: 1, flexGrow: 1, backgroundColor: colors.drawerBackground}}>
      <View
        style={{
          flexDirection: 'row',
          paddingTop:
            initialWindowMetrics.insets.top === 0
              ? 32
              : initialWindowMetrics.insets.top,
          backgroundColor: colors.drawerHeader,
          paddingBottom: 16,
        }}>
        <View
          style={{
            width: '70%',
          }}>
          <Wallet style={{marginLeft: 16}} />
          <Text
            numberOfLines={2}
            style={{
              color: colors.drawerHeaderWalletName,
              fontSize: 16,
              fontFamily: 'Montserrat-Medium',
              marginTop: 24,
              marginLeft: 18,
            }}>
            {wallet.getLabel()}
          </Text>
          <Text
            style={{
              color: colors.drawerHeaderWalletAmount,
              fontSize: 16,
              fontFamily: 'Montserrat-Regular',
              letterSpacing: 0,
              marginTop: 12,
              marginBottom: 4,
              marginLeft: 18,
            }}>
            {getFormattedWalletBalance(wallet.getBalance())} PND
          </Text>
        </View>
        <View
          style={{
            paddingTop: 16,
            paddingEnd: 16,
            width: '30%',
            alignItems: 'flex-end',
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              setDarkMode(!isDarkMode);
              console.log('Saving isDark = ' + !isDarkMode);
              saveTheme(!isDarkMode ? 'dark' : 'light');
              Animated.timing(animationProgress.current, {
                toValue: isDarkMode ? 1 : 0.5,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: false,
              }).start();
              SystemNavigationBar.setNavigationColor(
                // Hack: For some reason dark mode context doesn't change value instantly.
                !isDarkMode
                  ? themes.dark.backgroundGradientBottom
                  : themes.light.backgroundGradientBottom,
                true,
              );
            }}>
            <LottieView
              style={{
                width: 24,
                height: 24,
                resizeMode: 'contain',
              }}
              source={require('../lottie/47047-dark-mode-button.json')}
              autoPlay={false}
              progress={animationProgress.current}
              loop={false}
              resizeMode="cover"
            />
          </TouchableWithoutFeedback>
        </View>
      </View>
      <DrawerContentScrollView
        {...props}
        contentContainerStyle={{
          paddingTop: 2,
          flex: 1,
        }}>
        <DrawerItemList
          labelStyle={{
            marginLeft: -5,
            fontFamily: 'Montserrat-Medium',
            fontSize: 14,
            color: colors.drawerTextColor,
          }}
          itemStyle={{
            height: 50,
          }}
          state={newState}
          {...rest}
        />
      </DrawerContentScrollView>

      <SafeAreaView
        style={{
          backgroundColor: colors.drawer,
          marginLeft: 20,
          marginBottom: 20,
        }}>
        <TouchableOpacity
          onPress={() => {
            togglePndBlurbModal();
          }}
          style={{paddingVertical: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'Montserrat-Regular',
                marginLeft: 5,
                color: colors.drawerFooterText,
              }}>
              {strings.drawer.whatIsPandacoin}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            toggleHelpModal();
          }}
          style={{paddingVertical: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'Montserrat-Regular',
                marginLeft: 5,
                color: colors.drawerFooterText,
              }}>
              {strings.drawer.help}
            </Text>
          </View>
        </TouchableOpacity>
      </SafeAreaView>

      <WhatIsPandacoin
        toggleModal={togglePndBlurbModal}
        isVisible={pndBlurbModalVisible}
      />
      <Help toggleModal={toggleHelpModal} isVisible={helpModalVisible} />
    </View>
  );
};

export default BambooDrawer;
