import * as React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {ThemeContext} from '../contexts/ThemeStore';
import colors, {themes} from '../styles/colors';

export default function BambooBox({
  children,
  height,
  width,
  style,
  onPress,
  isT,
}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <TouchableOpacity
      style={[
        {
          height: height,
          width: '90%',
          backgroundColor: colors.boxColor,
          borderRadius: 10,
          flexDirection: 'row',
          shadowColor: colors.shadowColor,
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.17,
          shadowRadius: 2,
          elevation: 2.5,
        },
        style,
      ]}
      onPress={onPress}>
      {children}
    </TouchableOpacity>
  );
}
