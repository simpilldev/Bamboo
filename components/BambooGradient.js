import LinearGradient from 'react-native-linear-gradient';
import {View} from 'react-native';
import * as React from 'react';
import {initialWindowMetrics} from 'react-native-safe-area-context';
import {WalletSend} from './BambooSvgs.js';
import {darkTheme, lightTheme} from '../../styles/colors';
import {ThemeContext} from '../contexts/ThemeStore.js';
import {themes} from '../styles/colors.js';
import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings(require('../../locale.json'));

const BambooGradient = ({children}) => {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <LinearGradient
      // useAngle={true}
      // angle={90}
      // angleCenter={{x: 0.5, y: 0.5}}
      colors={[colors.backgroundGradientTop, colors.backgroundGradientBottom]}
      style={{flex: 1}}
      renderToHardwareTextureAndroid>
      <View style={{flex: 1}}>{children}</View>
    </LinearGradient>
  );
};

export default BambooGradient;
