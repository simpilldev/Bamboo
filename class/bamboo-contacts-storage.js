import SQLite from 'react-native-sqlite-storage';
import {Buffer} from 'buffer';
import '../../shim.js';
global.Buffer = global.Buffer || require('buffer').Buffer;

export default class ContactsDatabaseHelper {
  openDatabase() {
    return SQLite.openDatabase(
      {
        name: 'contacts_database',
        location: 'default',
        createFromLocation: '~data/contacts_database.sqlite',
      },
      () => {
        console.log('Database opened.');
      },
      error => {
        console.warn(
          'Error opening database (probably does not exist... creating). Error: ' +
            error,
        );
        return SQLite.openDatabase(
          {
            name: 'contacts_database',
            location: 'default',
          },
          () => {
            console.log('Database opened.');
            this.createTableIfEmpty();
          },
          error => {
            console.error('Error creating database : ' + error);
          },
        );
      },
    );
  }

  createTableIfEmpty() {
    this.openDatabase().transaction(tx => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS ' +
          'Contacts ' +
          '(label TEXT, address TEXT);',
        [],
        () => {
          console.log('Table created :D');
        },
        error => {
          console.error('Unable to create table: ' + error);
        },
      );
    });
  }

  getContacts() {
    return new Promise((resolve, reject) => {
      this.openDatabase().transaction(tx => {
        tx.executeSql(
          'SELECT label, address FROM Contacts',
          [],
          (tx, results) => {
            let labels = [];
            let addresses = [];
            if (results.rows.length > 0) {
              for (let i = 0; i < results.rows.length; i++) {
                labels[i] = results.rows.item(i).label;
                addresses[i] = results.rows.item(i).address;
              }
            }
            resolve({labels, addresses});
          },
          error => {
            console.error(error);
            reject(error);
          },
        );
      });
    });
  }

  // else if (!this.checkAddressValidity(address)) {
  //   console.log("Uh Oh, that doesn't look like a valid address.");
  //   return false;
  // }

  insertNewContact(label, address) {
    console.log('Label = ' + label);
    console.log('Address = ' + address);

    if (!this.checkAddressValidity(address)) {
      return false;
    } else {
      try {
        this.openDatabase().transaction(tx => {
          tx.executeSql(
            'INSERT INTO Contacts (label, address) VALUES (?,?)',
            [label, address],
            (tx, results) => {
              console.log('Inserted.');
            },
            error => {
              console.error(error);
            },
          );
        });
        return true;
      } catch (error) {
        console.error(error);
        return false;
      }
    }
  }

  deleteContact(address) {
    console.log('address = ' + address);

    try {
      this.openDatabase().transaction(tx => {
        tx.executeSql(
          'DELETE FROM Contacts WHERE Address = ? ',
          [address],
          (tx, results) => {
            console.log('Deleted.');
          },
          error => {
            console.error(error);
          },
        );
      });
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }

  checkAddressValidity(userAddress) {
    global.Buffer = global.Buffer || require('buffer').Buffer;
    const pandacoin = require('bitcoinjs-lib');

    try {
      pandacoin.address.toOutputScript(userAddress);
      return true;
    } catch (error) {
      console.log('The inputted address has no matching script :(');
      return false;
    }
  }
}
