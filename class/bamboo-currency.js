import BigDecimal from 'js-big-decimal';

export function convertPndToOtherCurrency(price, amount) {
  let convertedAmount;
  try {
    convertedAmount = new BigDecimal(amount).multiply(new BigDecimal(price));
    convertedAmount.compareTo(new BigDecimal(0.001)) === -1
      ? (convertedAmount = convertedAmount
          .round(8, BigDecimal.RoundingModes.HALF_UP)
          .getValue())
      : (convertedAmount = convertedAmount
          .round(2, BigDecimal.RoundingModes.HALF_UP)
          .getValue());
    return convertedAmount;
  } catch (error) {
    console.error(error);
  }
}

export function convertOtherCurrencyToPnd(price, amount) {
  let convertedAmount;
  try {
    convertedAmount = new BigDecimal(amount).divide(new BigDecimal(price));
    console.log('Converted Amount = ' + convertedAmount.getValue());
    convertedAmount.compareTo(new BigDecimal(10)) === -1
      ? (convertedAmount = convertedAmount
          .round(8, BigDecimal.RoundingModes.HALF_UP)
          .getValue())
      : (convertedAmount = convertedAmount
          .round(1, BigDecimal.RoundingModes.HALF_UP)
          .getValue());
    return convertedAmount;
  } catch (error) {
    console.error(error);
  }
}

export function getFormattedWalletBalance(balance) {
  let bal = new BigDecimal(balance).divide(new BigDecimal(1000000.0));

  switch (bal) {
    case bal.compareTo(new BigDecimal(0)) === 0:
      bal = 0;
      break;
    case (bal.compareTo(new BigDecimal(1)) === -1 &&
      bal.compareTo(new BigDecimal(0))) === 1:
      bal = bal.round(8);
      break;
    case bal.compareTo(new BigDecimal(10)) >= 0 &&
      bal.compareTo(new BigDecimal(10000)) <= 0:
      bal = bal.round(2);
      break;
    case bal.compareTo(new BigDecimal(Infinity)) <= 0 &&
      bal.compareTo(new BigDecimal(10000)) === 1:
      bal = bal.round(0);
      break;
    default:
      bal = bal.round(0);
      break;
  }

  return bal.getValue();
}

export function getWalletBalance(balance) {
  return new BigDecimal(balance).divide(new BigDecimal(1000000.0)).getValue();
}
