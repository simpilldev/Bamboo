import {AppStorage} from '../app-storage';
import {HDSegwitBech32Wallet} from './hd-segwit-bech32-wallet';
import {BlueStorageContext} from '../../blue_modules/storage-context';
import React, {useContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const GenerateBambooWallet = async () => {
  const {wallets, setWallets} = useContext(BlueStorageContext);

  const wallet = new HDSegwitBech32Wallet();
  await wallet.generate();

  console.log('Secret = ' + wallet.getSecret());
  wallet.validateMnemonic()
    ? console.log('Valid')
    : console.error('INVALID MNEMONIC');

  wallets.push(wallet);
  setWallets(wallets);

  const storage = new AppStorage();
  storage.wallets.push(wallet);
  await storage.saveToDisk();
  await wallet.fetchBalance();
  await wallet.fetchTransactions();

  wallet.getID();

  AsyncStorage.setItem('BambooWalletID', wallet.getID());
}

export async function LoadBambooWallet({setLoadingMsg}) {
  const Storage = new AppStorage();
  const wallet = Storage.wallets[0];
  setLoadingMsg('Loading Wallet Balance');
  await wallet.fetchBalance();
  setLoadingMsg('Loading Wallet Transactions');
  await wallet.fetchTransactions();

  // if (typeof secret !== 'undefined') {
  //   wallet.setSecret(secret);
  // } else {
  //   await wallet.generate();
  // }
}
