import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import colors, {themes} from '../../styles/colors.js';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import BambooGradient from '../../components/BambooGradient';
import {File, Paste, QrCode} from '../../components/BambooSvgs';
import {HelpImport, ImportFailedModal} from '../../components/BambooModals';
import 'react-native-screens';
import {ThemeContext} from '../../contexts/ThemeStore.js';
import Clipboard from '@react-native-clipboard/clipboard';
import startImport from '../../class/wallet-import.js';
import {BambooTextStyles} from '../../styles/textStyles.js';
import {BlueStorageContext} from '../../blue_modules/storage-context.js';
import OnAppLaunch from '../../class/on-app-launch.js';

const strings = new LocalizedStrings(require('../../locale.json'));

export default function ImportWalletScreen({route, navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const {setSelectedWallet, addAndSaveWallet} =
    React.useContext(BlueStorageContext);

  const [importText, setImportText] = React.useState('');

  React.useEffect(() => {
    if (route.params?.scannedText) {
      setImportText(route.params?.scannedText);
    }
  }, [route.params?.scannedText]);

  const [successModalVisible, setSuccessModalVisible] = React.useState(false);
  const toggleSuccessModal = () => {
    setSuccessModalVisible(!successModalVisible);
  };
  const [failureModalVisible, setFailureModalVisible] = React.useState(false);
  const toggleFailureModal = () => {
    setFailureModalVisible(!failureModalVisible);
  };
  const [helpModalVisible, setHelpModalVisible] = React.useState(false);
  const toggleHelpModal = () => {
    setHelpModalVisible(!helpModalVisible);
  };

  const [progress, setProgress] = React.useState();
  const onPassword = () => {
    console.log('WHERE THE FUCK IS THE PASSWORD??');
  };
  const onWallet = () => {
    console.log('WE FOUND DA WALLET LESSSGOOOOOO');
  };
  const onProgress = data => setProgress(data);

  return (
    <BambooGradient>
      <Text
        style={{
          fontFamily: 'Montserrat-SemiBold',
          paddingTop: 96,
          paddingLeft: 64,
          fontSize: 26,
          color: 'white',
        }}>
        {strings.screens.importScreen.title}
      </Text>

      <View
        style={{
          color: 'white',
          backgroundColor: colors.boxColor,
          height: '50%',
          marginTop: 24,
          marginHorizontal: 42,
          borderWidth: 1.5,
          borderRadius: 15,
          borderColor: colors.borderColor,
          textAlign: 'center',
          fontFamily: 'Montserrat-Regular',
          fontSize: 18,
        }}>
        <TextInput
          style={{
            padding: 24,
            fontFamily: 'Montserrat-Regular',
            fontSize: 16,
            color: 'white',
            flex: 0.9,
          }}
          autoFocus
          placeholder={strings.screens.importScreen.walletImportInstructions}
          defaultValue={importText}
          value={importText}
          onChangeText={setImportText}
          multiline
          textAlignVertical="top"
          placeholderTextColor="aliceblue"
          keyboardType="default"
          blurOnSubmit={false}
          underlineColorAndroid="#f000"
          returnKeyType="done"
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
          }}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
            }}
            onPress={async () => {
              setImportText(await Clipboard.getString());
            }}>
            <Paste />
            <Text
              style={{
                marginTop: 12,
                color: 'white',
                fontFamily: 'Montserrat-Regular',
                fontSize: 12,
              }}>
              {strings.screens.importScreen.pasteButton}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              alignItems: 'center',
            }}
            onPress={() => {
              navigation.navigate('ScanQr', {
                prevScreen: 'Import',
              });
            }}>
            <QrCode />
            <Text
              style={{
                marginTop: 12,
                color: 'white',
                fontFamily: 'Montserrat-Regular',
                fontSize: 12,
              }}>
              {strings.screens.importScreen.qrCodeButton}
            </Text>
          </TouchableOpacity>
          {/* <TouchableOpacity
            style={{
              alignItems: 'center',
            }}>
            <File />
            <Text
              style={{
                marginTop: 12,
                color: 'white',
                fontFamily: 'Montserrat-Regular',
                fontSize: 12,
              }}>
              {strings.screens.importScreen.fileButton}
            </Text>
          </TouchableOpacity> */}
        </View>
      </View>
      <Text style={BambooTextStyles().titleTextStyle}>PROGESS: {progress}</Text>

      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          paddingBottom: 40,
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{
            width: '80%',
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 18,
            borderRadius: 20,
            backgroundColor: colors.boxColor,
            shadowColor: colors.shadowColor,
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.17,
            shadowRadius: 2,
            elevation: 2.5,
          }}
          activeOpacity={0.5}
          onPress={() => {
            startImport(
              importText,
              false,
              false,
              onProgress,
              onWallet,
              onPassword,
            )
              .promise.then(async ({cancelled, stopped, wallets}) => {
                if (typeof wallets[0] !== 'undefined') {
                  wallets[0].setLabel('Imported Wallet ;)');
                  await addAndSaveWallet(wallets[0]);
                  await wallets[0].fetchBalance();
                  setSelectedWallet(wallets[0].getID());
                  await OnAppLaunch.setSelectedDefaultWallet(
                    wallets[0].getID(),
                  );
                  navigation.replace('HomeStack');
                }
              })
              .catch(e => {
                console.error('Import error = ' + e);
              });
          }}>
          <Text
            style={{
              color: 'white',
              fontFamily: 'Montserrat-Regular',
              letterSpacing: 0.1,
              fontSize: 18,
            }}>
            {strings.screens.importScreen.importWalletButton}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={toggleHelpModal}>
          <Text
            style={{
              marginTop: 16,
              color: 'white',
              fontFamily: 'Montserrat-Regular',
              letterSpacing: 0.1,
              fontSize: 14,
            }}>
            {strings.screens.importScreen.cannotImportButton}
          </Text>
        </TouchableOpacity>
      </View>

      <ImportFailedModal
        toggleModal={toggleFailureModal}
        isVisible={failureModalVisible}
      />

      <HelpImport toggleModal={toggleHelpModal} isVisible={helpModalVisible} />
    </BambooGradient>
  );
}
