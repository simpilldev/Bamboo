import {Alert, Text, TouchableOpacity, _View} from 'react-native';
import * as React from 'react';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import {ThemeContext} from '../../contexts/ThemeStore';
import {themes} from '../../styles/colors';
import LocalizedStrings from 'react-native-localization';
import {BambooTextStyles} from '../../styles/textStyles';
import {
  AddWalletButton,
  CancelQrScanButton,
} from '../../components/BambooFooterItem';
import {RNCamera} from 'react-native-camera';
import QRCodeScanner from 'react-native-qrcode-scanner';

const strings = new LocalizedStrings(require('../../locale.json'));

export default function ScanQRCodeScreen({navigation, route}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const {prevScreen} = route.params;

  const onSuccess = e => {
    console.log('SCANNED');
    navigation.navigate({
      name: prevScreen,
      params: {scannedText: e.data},
      merge: true,
    });
  };

  const getTitle = () => {
    if (prevScreen === 'Send')
      return strings.screens.scanQrCodeScreen.titleAddress;
    else if (prevScreen === 'Import')
      return strings.screens.scanQrCodeScreen.titleImport;
  };

  return (
    <QRCodeScanner
      onRead={onSuccess}
      cameraType="back"
      permissionDialogTitle="Permission needed for QR scan"
      permissionDialogMessage="Bamboo needs the camera permission to be able to scan QR codes."
      vibrate
      flashMode={RNCamera.Constants.FlashMode.auto}
      topContent={
        <Text style={BambooTextStyles().regularTextStyle}>{getTitle()}</Text>
      }
      bottomContent={
        <CancelQrScanButton
          navigation={navigation}
          onPress={() => {
            navigation.navigate({
              name: prevScreen,
              merge: true,
            });
          }}
        />
      }
    />
  );
}
