import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  _View,
  TextInput,
  Alert,
} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import colors, {themes} from '../../styles/colors';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import {
  Bag,
  ContactBook,
  Paste,
  QrCode,
  WalletSend,
} from '../../components/BambooSvgs';
import {
  CreatingTxModal,
  InvalidAddressModal,
  InvalidAmountModal,
  InvalidLabelModal,
  MissingEntryModal,
  SendConfirmationModal,
} from '../../components/BambooModals';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import {initialWindowMetrics} from 'react-native-safe-area-context';
import {ThemeContext} from '../../contexts/ThemeStore';
import BambooFooterItem, {
  CreateTxButton,
} from '../../components/BambooFooterItem';
import {BambooTextStyles} from '../../styles/textStyles';
import {
  HDSegwitBech32Transaction,
  HDSegwitBech32Wallet,
  WatchOnlyWallet,
} from '../../class';
import {BlueStorageContext} from '../../blue_modules/storage-context';
import Clipboard from '@react-native-clipboard/clipboard';
import NetworkTransactionFees from '../../models/networkTransactionFees';
import {AbstractHDElectrumWallet} from '../../class/wallets/abstract-hd-electrum-wallet';
import {sleep} from '../../tests/e2e/helperz';
import {
  getFormattedWalletBalance,
  getWalletBalance,
} from '../../class/bamboo-currency';
import BigDecimal from 'js-big-decimal';
const BlueElectrum = require('../../blue_modules/BlueElectrum');
const localeFile = require('../../locale.json');
const strings = new LocalizedStrings(localeFile);

export default function SendScreen({navigation, route}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const {wallets, selectedWallet} = React.useContext(BlueStorageContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const wallet = wallets.find(w => selectedWallet === w.getID());

  const [changeAddress, setChangeAddress] = React.useState();

  React.useEffect(() => {
    const getChangeAddressAsync = async () => {
      console.log('Finding change address of wallet: ' + wallet.getLabel()); // cache

      let change;
      if (WatchOnlyWallet.type === wallet.type && !wallet.isHd()) {
        // plain watchonly - just get the address
        change = wallet.getAddress();
      } else {
        // otherwise, lets call widely-used getChangeAddressAsync()
        try {
          change = await Promise.race([
            sleep(2000),
            wallet.getChangeAddressAsync(),
          ]);
        } catch (_) {}

        if (!change) {
          // either sleep expired or getChangeAddressAsync threw an exception
          if (wallet instanceof AbstractHDElectrumWallet) {
            change = wallet._getInternalAddressByIndex(
              wallet.getNextFreeChangeAddressIndex(),
            );
          } else {
            // legacy wallets
            change = wallet.getAddress();
          }
        }
      }

      if (change) setChangeAddress(change); // cache

      console.log('Change: ' + change);

      return change;
    };
    getChangeAddressAsync().then(address => {
      setChangeAddress(address);
    });
  }, [wallet]);

  const [sendModalVisible, setSendModalVisible] = React.useState(false);
  const toggleSendModal = () => {
    setSendModalVisible(!sendModalVisible);
  };
  const [missingEntryModalVisible, setMissingEntryModalVisible] =
    React.useState(false);
  const toggleMissingEntryModal = () => {
    setMissingEntryModalVisible(!missingEntryModalVisible);
  };
  const [invalidAddressModalVisible, setInvalidAddressModalVisible] =
    React.useState(false);
  const toggleInvalidAddressModal = () => {
    setInvalidAddressModalVisible(!invalidAddressModalVisible);
  };
  const [invalidAmountModalVisible, setInvalidAmountModalVisible] =
    React.useState(false);
  const toggleInvalidAmountModal = () => {
    setInvalidAmountModalVisible(!invalidAmountModalVisible);
  };
  const [invalidLabelModalVisible, setInvalidLabelModalVisible] =
    React.useState(false);
  const toggleInvalidLabelModal = () => {
    setInvalidLabelModalVisible(!invalidLabelModalVisible);
  };
  const [creatingTxModalVisible, setCreatingTxModalVisible] =
    React.useState(false);
  const toggleCreatingTxModal = () => {
    setCreatingTxModalVisible(!creatingTxModalVisible);
  };

  const [address, setAddress] = React.useState(
    'PPtN11aS7hRpaDrP7GyvQeWzXbdts8Hrsx',
  );
  const [addressValid, setAddressValid] = React.useState(false);

  const [amount, setAmount] = React.useState('');
  const [amountValid, setAmountValid] = React.useState(false);

  const [label, setLabel] = React.useState('');
  const [labelValid, setLabelValid] = React.useState(false);

  const areBoxesEmpty = () => {
    if (address.length === 0 || amount.length === 0) {
      toggleMissingEntryModal();
      return true;
    } else {
      return false;
    }
  };

  const isValid = () => {
    if (!isAddressValid(address)) {
      toggleInvalidAddressModal();
      return false;
    } else if (!isAmountValid(amount)) {
      toggleInvalidAmountModal();
      return false;
    } else if (!isLabelValid(label)) {
      toggleInvalidLabelModal();
      return false;
    } else {
      return true;
    }
  };

  const isAddressValid = address => {
    global.Buffer = global.Buffer || require('buffer').Buffer;
    const pandacoin = require('bitcoinjs-lib');

    console.log('Checking if address is valid: ' + address);
    try {
      pandacoin.address.toOutputScript(address);
      setAddressValid(true);
      return true;
    } catch (error) {
      setAddressValid(false);
      return false;
    }
  };

  const isAmountValid = amount => {
    let userAmount;
    let walletBalance = new BigDecimal(getWalletBalance(wallet.getBalance()));

    try {
      userAmount = new BigDecimal(amount);
      walletBalance = new BigDecimal(getWalletBalance(wallet.getBalance()));
    } catch {
      Alert.alert('NaN', 'AYOOOO, WTF IS THIS SHIT?');
      return;
    }
    console.log('Wallet balance in send = ' + walletBalance);
    if (
      userAmount.compareTo(walletBalance) === 1 ||
      userAmount.compareTo(new BigDecimal(0)) <= 0
    ) {
      setAmountValid(false);
    } else setAmountValid(true);
  };

  const isLabelValid = label => {
    if (label.match(/[^0-9a-z]/i) === null) {
      setLabelValid(true);
    } else {
      setLabelValid(false);
    }
  };

  const getAddressBorderColor = () => {
    if (address.length === 0) return colors.boxColor;
    if (!addressValid && address.length > 0) return colors.inputIncorrect;
    if (addressValid && address.length > 0) return colors.inputCorrect;
  };

  const getAmountBorderColor = () => {
    if (amount.length === 0) return colors.boxColor;
    if (!amountValid && amount.length > 0) return colors.inputIncorrect;
    if (amountValid && amount.length > 0) return colors.inputCorrect;
  };

  const getLabelBorderColor = () => {
    if (label.length === 0) return colors.boxColor;
    if (!labelValid && label.length > 0) return colors.inputIncorrect;
    if (labelValid && label.length > 0) return colors.inputCorrect;
  };

  const addressInputRef = React.useRef();
  const amountInputRef = React.useRef();
  const labelInputRef = React.useRef();

  React.useEffect(() => {
    if (route.params?.scannedText) {
      setAddress(route.params?.scannedText);
      isAddressValid(route.params?.scannedText);
    }
  }, [route.params?.scannedText]);

  // return loading ? (
  //   <BambooGradient>
  //     <BambooHeader
  //       navigation={navigation}
  //       title={strings.screens.sendScreen.title}
  //     />
  //   </BambooGradient>
  // ) : (
  return (
    <BambooGradient>
      <KeyboardAwareScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: 'space-between',
          flexDirection: 'column',
        }}>
        <BambooHeader
          navigation={navigation}
          title={strings.screens.sendScreen.title}
        />
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            marginTop: 20,
            marginHorizontal: 32,
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '100%',
              marginVertical: 16,
              justifyContent: 'space-around',
              borderRadius: 10,
              backgroundColor: colors.boxColor,
              borderColor: getAddressBorderColor(),
              borderWidth: 2.5,
              height: 100,
              flexDirection: 'row',
              alignItems: 'center',
              shadowColor: colors.shadowColor,
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.17,
              shadowRadius: 2,
              elevation: 2.5,
            }}>
            <View
              style={{
                flex: 0.75,
                flexDirection: 'column',
              }}>
              <TextInput
                ref={addressInputRef}
                autoComplete="off"
                multiline
                enablesReturnKeyAutomatically
                value={address}
                onChangeText={address => {
                  setAddress(address);
                  isAddressValid(address);
                }}
                autoFocus
                onSubmitEditing={() => {
                  amountInputRef.current.focus();
                }}
                style={[BambooTextStyles().smallTextStyle, {marginStart: 16}]}
                placeholder={strings.screens.sendScreen.address}
                placeholderTextColor={colors.primaryTextColor}
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                underlineColorAndroid="#f000"
                blurOnSubmit
              />
            </View>
            <View
              style={{
                btnContainer: {
                  flex: 0.25,
                  marginRight: 24,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'flex-end',
                },
              }}>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={async () => {
                    setAddress(await Clipboard.getString());
                  }}
                  style={{marginRight: 4}}>
                  <Paste />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{marginLeft: 4}}
                  onPress={() => {
                    navigation.navigate('ScanQr', {
                      prevScreen: 'Send',
                    });
                  }}>
                  <QrCode />
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View
            style={{
              marginVertical: 16,
              justifyContent: 'space-around',
              borderRadius: 10,
              backgroundColor: colors.boxColor,
              borderColor: getAmountBorderColor(),
              borderWidth: 2.5,
              height: 100,
              flexDirection: 'row',
              alignItems: 'center',
              shadowColor: colors.shadowColor,
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.17,
              shadowRadius: 2,
              elevation: 2.5,
            }}>
            <View
              style={{
                flex: 0.75,
                flexDirection: 'column',
              }}>
              <TextInput
                ref={amountInputRef}
                onChangeText={amount => {
                  setAmount(amount);
                  isAmountValid(amount);
                }}
                onSubmitEditing={() => {
                  labelInputRef.current.focus();
                }}
                style={[
                  BambooTextStyles().smallTextStyle,
                  {marginTop: 8, marginHorizontal: 32},
                ]}
                placeholder={strings.screens.sendScreen.amount}
                placeholderTextColor="white"
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                underlineColorAndroid="#f000"
                blurOnSubmit={false}
              />
            </View>
            <View
              style={{
                flex: 0.25,
                marginRight: 24,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              <TouchableOpacity>
                <Bag />
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              marginTop: 16,
              marginBottom: 48,
              justifyContent: 'space-around',
              borderRadius: 10,
              backgroundColor: colors.boxColor,
              borderColor: getLabelBorderColor(),
              borderWidth: 2.5,
              height: 100,
              flexDirection: 'row',
              alignItems: 'center',
              shadowColor: colors.shadowColor,
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.17,
              shadowRadius: 2,
              elevation: 2.5,
            }}>
            <View
              style={{
                flex: 0.75,
                flexDirection: 'column',
              }}>
              <TextInput
                ref={labelInputRef}
                onChangeText={label => {
                  setLabel(label);
                  isLabelValid(label);
                }}
                onSubmitEditing={() => {}}
                style={[
                  BambooTextStyles().smallTextStyle,
                  {marginTop: 8, marginHorizontal: 32},
                ]}
                placeholder={strings.screens.sendScreen.label}
                placeholderTextColor="white"
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                underlineColorAndroid="#f000"
                blurOnSubmit
              />
            </View>
            <View
              style={{
                flex: 0.25,
                marginRight: 24,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              <TouchableOpacity>
                <ContactBook />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Text
              onPress={toggleCreatingTxModal}
              style={BambooTextStyles().titleTextStyle}>
              CREATE TX
            </Text>
          </View>
          <CreateTxButton
            toggleModal={toggleCreatingTxModal}
            toggleInvalidAddressModal={toggleInvalidAddressModal}
            isVisible={creatingTxModalVisible}
            isValid={isValid}
            areBoxesEmpty={areBoxesEmpty}
          />
        </View>
        <Text style={BambooTextStyles().titleTextStyle}>{changeAddress}</Text>
        <MissingEntryModal
          toggleModal={toggleMissingEntryModal}
          isVisible={missingEntryModalVisible}
        />
        <InvalidAddressModal
          toggleModal={toggleInvalidAddressModal}
          isVisible={invalidAddressModalVisible}
        />
        <InvalidAmountModal
          toggleModal={toggleInvalidAmountModal}
          isVisible={invalidAmountModalVisible}
        />
        <InvalidLabelModal
          toggleModal={toggleInvalidLabelModal}
          isVisible={invalidLabelModalVisible}
        />
        <SendConfirmationModal
          toggleModal={toggleSendModal}
          isVisible={sendModalVisible}
          amount={amount}
          address={address}
        />
        <CreatingTxModal
          toggleModal={toggleCreatingTxModal}
          isVisible={creatingTxModalVisible}
          amount={amount}
          address={address}
          wallet={wallet}
        />
      </KeyboardAwareScrollView>
    </BambooGradient>
  );
}
