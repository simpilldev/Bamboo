import {StyleSheet, View, Text, TouchableOpacity, Animated} from 'react-native';
import colors, {themes} from '../../styles/colors';
import * as React from 'react';
import LottieView from 'lottie-react-native';
import textStyles from '../../styles/textStyles';
import {SeedPhraseWarningModal} from '../../components/BambooModals';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import {SeedWordsColumn} from '../../components/BambooSeedWords';
import {initialWindowMetrics} from 'react-native-safe-area-context';
import LocalizedStrings from 'react-native-localization';
import {SeedWarningButton} from '../../components/BambooFooterItem';
import {ThemeContext} from '../../contexts/ThemeStore';

const localeFile = require('../../locale.json');
const strings = new LocalizedStrings(localeFile);

export default function SeedScreen({navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const [seedPhraseModalVisible, setSeedPhraseModalVisible] =
    React.useState(false);
  const toggleSeedPhraseModal = () => {
    setSeedPhraseModalVisible(!seedPhraseModalVisible);
  };

  return (
    <BambooGradient>
      <BambooHeader
        navigation={navigation}
        title={strings.screens.seedScreen.title}
      />
      <SeedWordsColumn />

      <SeedWarningButton toggleModal={toggleSeedPhraseModal} />

      <SeedPhraseWarningModal
        toggleModal={toggleSeedPhraseModal}
        isVisible={seedPhraseModalVisible}
      />
    </BambooGradient>
  );
}
