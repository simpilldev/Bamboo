import {View, Text, ScrollView, RefreshControl} from 'react-native';
// import {useBackHandler} from '@react-native-community/hooks';
import * as React from 'react';
import BambooHeader from '../../components/BambooHeader';
import BambooGradient from '../../components/BambooGradient';
import {BambooToastModal, NoInternetModal} from '../../components/BambooModals';
import {BlueStorageContext} from '../../blue_modules/storage-context';
import LocalizedStrings from 'react-native-localization';
import {ReceiveButton, SendButton} from '../../components/BambooFooterItem';
import {ThemeContext} from '../../contexts/ThemeStore';
import {themes} from '../../styles/colors';
import {BambooTextStyles} from '../../styles/textStyles';
import {getFormattedWalletBalance} from '../../class/bamboo-currency';
import {HDSegwitBech32Wallet} from '../../class';

const strings = new LocalizedStrings(require('../../locale.json'));

export default function HomeScreen({navigation, route}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const {wallets, selectedWallet} = React.useContext(BlueStorageContext);

  const [wallet, setWallet] = React.useState(
    wallets.find(w => selectedWallet === w.getID()),
  );

  React.useEffect(() => {
    setWallet(wallets.find(w => selectedWallet === w.getID()));
    setBalance(getFormattedWalletBalance(wallet.getBalance()));
  }, [wallet, wallets, selectedWallet]);

  const [balance, setBalance] = React.useState(
    getFormattedWalletBalance(wallet.getBalance()),
  );
  const [unconfirmedBalance, setUnconfirmedBalance] = React.useState(
    getFormattedWalletBalance(wallet.getUnconfirmedBalance()),
  );
  const [ticker, setTicker] = React.useState('PND');

  const [testToastModalVisible, setTestToastModalVisible] =
    React.useState(false);
  const toggleTestToastModal = () => {
    setTestToastModalVisible(!testToastModalVisible);
  };

  React.useEffect(() => {}, []);
  const [noInternetModalVisible, setNoInternetModalVisible] =
    React.useState(false);
  const toggleNoInternetModal = () => {
    setNoInternetModalVisible(!noInternetModalVisible);
  };

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wallet.fetchBalance().then(() => {
      setBalance(getFormattedWalletBalance(wallet.getBalance()));
      setUnconfirmedBalance(
        getFormattedWalletBalance(wallet.getUnconfirmedBalance()),
      );
      setRefreshing(false);
    });
  }, [wallet]);

  return (
    <BambooGradient>
      <BambooHeader navigation={navigation} />
      <ScrollView
        contentContainerStyle={{flex: 1, flexGrow: 1, flexDirection: 'column'}}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View
          style={{
            flex: 1,
            flexGrow: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            onPress={() => {
              toggleTestToastModal();
            }}
            style={BambooTextStyles().pndTickerStyle}>
            {ticker}
          </Text>
          <Text style={BambooTextStyles().pndBalanceStyle}>{balance}</Text>
          <Text style={BambooTextStyles().smallTextStyle}>
            Unconfirmed: {unconfirmedBalance}
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}>
          <SendButton navigation={navigation} />
          <ReceiveButton navigation={navigation} />
        </View>
      </ScrollView>

      <NoInternetModal
        toggleModal={toggleNoInternetModal}
        isVisible={noInternetModalVisible}
      />

      <BambooToastModal
        toggleModal={toggleTestToastModal}
        isVisible={testToastModalVisible}
        isTop
      />
    </BambooGradient>
  );
}
