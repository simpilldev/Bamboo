import * as React from 'react';
import {View} from 'react-native';
import LocalizedStrings from 'react-native-localization';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import {About, General, Security} from '../../components/BambooSettingsBox';
import {ThemeContext} from '../../contexts/ThemeStore';
import {themes} from '../../styles/colors';

const strings = new LocalizedStrings(require('../../locale.json'));

export default function SettingsScreen({navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  return (
    <BambooGradient>
      <BambooHeader
        navigation={navigation}
        title={strings.screens.settingsScreen.title}
      />
      <View style={{marginTop: 16, marginHorizontal: 32}}>
        <General />
        <Security navigation={navigation} />
        <About />
      </View>
    </BambooGradient>
  );
}
