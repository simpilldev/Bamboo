import * as React from 'react';
import {TouchableOpacity, View} from 'react-native';
import LocalizedStrings from 'react-native-localization';
import {Text} from 'react-native-svg';
import {BlueStorageContext} from '../../blue_modules/storage-context';
import Biometric from '../../class/biometrics';
import BambooBox from '../../components/BambooBox';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import {EncryptModal} from '../../components/BambooModals';
import {About, General, Security} from '../../components/BambooSettingsBox';
import {Cog} from '../../components/BambooSvgs';
import BambooSwitch from '../../components/BambooSwitch';
import {ThemeContext} from '../../contexts/ThemeStore';
import {themes} from '../../styles/colors';

const strings = new LocalizedStrings(require('../../locale.json'));

export default function SecurityScreen({navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const [encryptionModalVisible, setEncryptionModalVisible] =
    React.useState(false);
  const toggleEncryptionModal = () => {
    setEncryptionModalVisible(!encryptionModalVisible);
  };

  const [decryptionModalVisible, setDecryptionModalVisible] =
    React.useState(false);
  const toggleDecryptionModal = () => {
    setDecryptionModalVisible(!decryptionModalVisible);
  };

  const [encryptionSwitchEnabled, setEncryptionSwitchEnabled] =
    React.useState(false);

  React.useEffect(() => {
    const isEncrypted = async () => {
      const enc = await isStorageEncrypted();
      console.log('isEncrypted = ' + enc);
      setEncryptionSwitchEnabled(enc);
      setIsLoading(false);
    };
    isEncrypted();
  }, []);

  const {isStorageEncrypted, encryptStorage, decryptStorage, saveToDisk} =
    React.useContext(BlueStorageContext);
  const [isLoading, setIsLoading] = React.useState(true);
  const [biometrics, setBiometrics] = React.useState({
    isDeviceBiometricCapable: false,
    isBiometricsEnabled: false,
    biometricsType: '',
  });

  // const fetchBiometrics = async () => {
  //   const isBiometricsEnabled = await Biometric.isBiometricUseEnabled();
  //   const isDeviceBiometricCapable = await Biometric.isDeviceBiometricCapable();
  //   const biometricsType = await Biometric.biometricType();
  //   const isStorageEncryptedSwitchEnabled = await isStorageEncrypted();
  //   console.log(
  //     isBiometricsEnabled +
  //       ', ' +
  //       isDeviceBiometricCapable +
  //       ', ' +
  //       biometricsType +
  //       ', ' +
  //       isStorageEncryptedSwitchEnabled,
  //   );
  //   setStorageIsEncryptedSwitchEnabled(isStorageEncryptedSwitchEnabled);
  //   setBiometrics({
  //     isBiometricsEnabled,
  //     isDeviceBiometricCapable,
  //     biometricsType,
  //   });
  // };

  // const onUseBiometricSwitch = async value => {
  //   const isBiometricsEnabled = {
  //     isDeviceBiometricCapable: biometrics.isDeviceBiometricCapable,
  //     isBiometricsEnabled: biometrics.isBiometricsEnabled,
  //     biometricsType: biometrics.biometricsType,
  //   };
  //   if (await Biometric.unlockWithBiometrics()) {
  //     isBiometricsEnabled.isBiometricsEnabled = value;
  //     await Biometric.setBiometricUseEnabled(value);
  //     setBiometrics(isBiometricsEnabled);
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // };

  return isLoading ? (
    <BambooGradient>
      <BambooHeader
        navigation={navigation}
        title={strings.screens.securityScreen.title}
      />
    </BambooGradient>
  ) : (
    <BambooGradient>
      <BambooHeader
        navigation={navigation}
        title={strings.screens.securityScreen.title}
      />
      <View
        style={{
          paddingHorizontal: 32,
        }}>
        <BambooSwitch
          text={strings.screens.securityScreen.encryptStorage}
          onToggle={() => {
            encryptionSwitchEnabled
              ? toggleDecryptionModal()
              : toggleEncryptionModal();
          }}
          isEnabled={encryptionSwitchEnabled}
        />
      </View>
      <EncryptModal
        toggleModal={toggleEncryptionModal}
        isVisible={encryptionModalVisible}
        setEncryptionSwitchEnabled={setEncryptionSwitchEnabled}
      />
    </BambooGradient>
  );
}
