import {
  StyleSheet,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
  TextInput,
} from 'react-native';
import colors, {themes} from '../../styles/colors.js';
import {BambooTextStyles} from '../../styles/textStyles.js';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import BambooGradient from '../../components/BambooGradient';
import {Eye} from '../../components/BambooSvgs';
import {
  EmptyPasswordModal,
  PasswordsDoNotMatchModal,
  WeakPasswordModal,
} from '../../components/BambooModals.js';
import 'react-native-screens';
import {useNavigation} from '@react-navigation/native';
import {ThemeContext} from '../../contexts/ThemeStore.js';

const strings = new LocalizedStrings(require('../../locale.json'));

export default function CreatePasswordScreen({navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const [badPasswordModalVisible, setBadPasswordModalVisible] =
    React.useState(false);
  const toggleBadPasswordModal = () => {
    setBadPasswordModalVisible(!badPasswordModalVisible);
  };

  const [passwordsDoNotMatchModalVisible, setPasswordsDoNotMatchModalVisible] =
    React.useState(false);
  const togglePasswordsDoNotMatchModal = () => {
    setPasswordsDoNotMatchModalVisible(!passwordsDoNotMatchModalVisible);
  };

  const [emptyPasswordModalVisible, setEmptyPasswordModalVisible] =
    React.useState(false);
  const toggleEmptyPasswordModal = () => {
    setEmptyPasswordModalVisible(!emptyPasswordModalVisible);
  };

  const [password, setPassword] = React.useState('');
  const [password2, setPassword2] = React.useState('');

  const passwordsMatch = () => {
    if (password.length <= 0) {
      toggleEmptyPasswordModal();
      return false;
    } else if (password === password2) {
      return true;
    } else {
      togglePasswordsDoNotMatchModal();
      return false;
    }
  };

  const isStrong = () => {
    console.log('Password length = ' + password.length);
    if (password.length >= 6) {
      return true;
    } else {
      toggleBadPasswordModal();
      return false;
    }
  };

  console.log('User Password = ' + password);
  console.log('User Password2 = ' + password2);

  const [arePasswordsVisible, setArePasswordsVisible] = React.useState(true);
  console.log('Val = ' + arePasswordsVisible);

  const changePwdType = () => {
    setArePasswordsVisible(!arePasswordsVisible);
  };

  return (
    <BambooGradient>
      <View
        style={{
          marginTop: 96,
          marginHorizontal: 32,
        }}>
        <Text style={BambooTextStyles().titleTextStyle}>Safety First.</Text>

        <Text
          style={[
            {marginTop: 32, marginBottom: 80},
            BambooTextStyles().regularTextStyle,
          ]}>
          Please choose a password for your Pandacoin wallet.
        </Text>
        <View
          style={{
            marginTop: 24,
            marginVertical: 16,
            justifyContent: 'space-around',
            borderRadius: 20,
            backgroundColor: colors.boxColor,
            borderColor: colors.borderColor,
            borderWidth: 1.5,
            height: 80,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{flex: 0.75, flexDirection: 'column'}}>
            <TextInput
              style={[
                BambooTextStyles().smallTextStyle,
                {flex: 1, paddingStart: 24, textAlignVertical: 'center'},
              ]}
              placeholder={strings.screens.createPassScreen.enterPassword}
              placeholderTextColor="white"
              value={password}
              onChangeText={password => setPassword(password)}
              secureTextEntry={arePasswordsVisible}
              multiline={false}
              autoCapitalize="none"
              autoCorrect={false}
              textContentType="newPassword"
              keyboardType="default"
              enablesReturnKeyAutomatically
              returnKeyType="next"
              underlineColorAndroid="#f000"
              blurOnSubmit={false}
            />
          </View>
          <TouchableOpacity
            style={{
              height: '100%',
              flex: 0.25,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={changePwdType}>
            <Eye arePasswordsVisible={arePasswordsVisible} />
          </TouchableOpacity>
        </View>

        <View
          style={{
            marginTop: 24,
            marginVertical: 16,
            justifyContent: 'space-around',
            borderRadius: 20,
            backgroundColor: colors.boxColor,
            borderColor: colors.borderColor,
            borderWidth: 1.5,
            height: 80,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{flex: 0.75, flexDirection: 'column'}}>
            <TextInput
              style={[
                BambooTextStyles().smallTextStyle,
                {flex: 1, paddingStart: 24, textAlignVertical: 'center'},
              ]}
              placeholder={strings.screens.createPassScreen.reenterPassword}
              placeholderTextColor="white"
              value={password2}
              onChangeText={password2 => setPassword2(password2)}
              secureTextEntry={arePasswordsVisible}
              multiline={false}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="default"
              returnKeyType="next"
              underlineColorAndroid="#f000"
              blurOnSubmit={false}
            />
          </View>
          <TouchableOpacity
            style={{
              height: '100%',
              flex: 0.25,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={changePwdType}>
            <Eye arePasswordsVisible={arePasswordsVisible} />
          </TouchableOpacity>
        </View>
      </View>

      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          marginBottom: 40,
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{
            width: '80%',
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 18,
            borderRadius: 20,
            backgroundColor: colors.boxColor,
            shadowColor: colors.shadowColor,
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.17,
            shadowRadius: 2,
            elevation: 2.5,
          }}
          activeOpacity={0.5}
          onPress={() => {
            if (passwordsMatch() && isStrong()) {
              navigation.replace('HomeStack');
            }
          }}>
          <Text
            style={{
              color: 'white',
              fontFamily: 'Montserrat-Regular',
              letterSpacing: 0.1,
              fontSize: 18,
            }}>
            {strings.screens.createPassScreen.createPassword}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text
            style={{
              marginTop: 16,
              color: 'white',
              fontFamily: 'Montserrat-Regular',
              letterSpacing: 0.1,
              fontSize: 14,
            }}>
            {strings.screens.createPassScreen.passwordAdvice}
          </Text>
        </TouchableOpacity>
      </View>

      <PasswordsDoNotMatchModal
        toggleModal={togglePasswordsDoNotMatchModal}
        isVisible={passwordsDoNotMatchModalVisible}
      />

      <WeakPasswordModal
        toggleModal={toggleBadPasswordModal}
        isVisible={badPasswordModalVisible}
      />

      <EmptyPasswordModal
        toggleModal={toggleEmptyPasswordModal}
        isVisible={emptyPasswordModalVisible}
      />
    </BambooGradient>
  );
}
