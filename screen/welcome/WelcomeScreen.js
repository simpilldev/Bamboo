import {themes} from '../../styles/colors.js';
import React, {useState} from 'react';
import LocalizedStrings from 'react-native-localization';
import BambooGradient from '../../components/BambooGradient';
import {BigWallet} from '../../components/BambooSvgs';

import {Text, Keyboard, View, StyleSheet, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {HDSegwitBech32Wallet, AppStorage} from '../../class';
import {useNavigation} from '@react-navigation/native';
import loc from '../../loc';
import {BlueStorageContext} from '../../blue_modules/storage-context';
import {Shadow} from 'react-native-shadow-2';
import {ThemeContext} from '../../contexts/ThemeStore.js';
import {
  BambooWallet,
  GenerateBambooWallet,
} from '../../class/wallets/BambooWallet.js';
import {
  BambooLoadingModal,
  GenerateWalletModal,
  WalletLabel,
} from '../../components/BambooModals.js';
import {initialWindowMetrics} from 'react-native-safe-area-context';

let localeFile = require('../../locale.json');
let strings = new LocalizedStrings(localeFile);

const testWalletID =
  'a2991676c70281d48a659639e3f25612e9032a0f18f59b77c6f1a3738e6c1c96';

export default function WelcomeScreen({navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);

  const [generatingWalletModalVisible, setGeneratingWalletModalVisible] =
    React.useState(false);
  const toggleGeneratingWalletModal = () => {
    setGeneratingWalletModalVisible(!generatingWalletModalVisible);
  };
  const goToHome = () => {
    navigation.replace('HomeStack');
  };

  const [label, setLabel] = useState('');

  const [labelModalVisible, setLabelModalVisible] = React.useState(false);
  const toggleLabelModal = () => {
    setLabelModalVisible(!labelModalVisible);
  };

  const colors = isDarkMode ? themes.dark : themes.light;

  const onFinish = label => {
    console.log(typeof label);
    toggleGeneratingWalletModal(label);
  };

  return (
    <BambooGradient>
      <View style={styles.centerStyle}>
        <BigWallet />
        <Text style={styles.welcomeTextStyle}>
          {strings.screens.welcomeScreen.welcomeToBamboo}
        </Text>
        <Text style={styles.manageTextStyle}>
          {strings.screens.welcomeScreen.description}
        </Text>
      </View>

      <View style={styles.footerStyle}>
        <TouchableOpacity
          activeOpacity={0.5}
          style={{
            width: '80%',
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 18,
            borderRadius: 20,
            backgroundColor: colors.boxColor,
            shadowColor: colors.shadowColor,
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.17,
            shadowRadius: 2,
            elevation: 2.5,
          }}
          onPress={() => {
            toggleLabelModal();
          }}>
          <Text style={styles.createWalletTextStyle}>
            {strings.screens.welcomeScreen.createWallet}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.importWalletButtonStyle}
          onPress={() => {
            navigation.navigate('Import');
          }}>
          <Text style={styles.importWalletTextStyle}>
            {strings.screens.welcomeScreen.importWallet}
          </Text>
        </TouchableOpacity>
      </View>

      <GenerateWalletModal
        toggleModal={toggleGeneratingWalletModal}
        isVisible={generatingWalletModalVisible}
        label={label}
        goToHome={goToHome}
      />

      <WalletLabel
        toggleModal={toggleLabelModal}
        isVisible={labelModalVisible}
        onFinish={onFinish}
        label={label}
        setLabel={setLabel}
      />
    </BambooGradient>
  );
}
const styles = StyleSheet.create({
  createWalletTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    fontSize: 18,
  },

  importWalletButtonStyle: {
    marginTop: 16,
    marginBottom: 40 - initialWindowMetrics.insets.bottom * 0.25,
  },

  importWalletTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    fontSize: 14,
  },

  welcomeTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    paddingTop: 50,
    fontSize: 26,
  },

  manageTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Light',
    letterSpacing: 0.1,
    marginHorizontal: 32,
    paddingTop: 25,
    fontSize: 19,
  },

  centerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },

  centerImageStyle: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
  },

  footerStyle: {
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 40,
    alignItems: 'center',
  },
});
