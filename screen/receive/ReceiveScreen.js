import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import colors, {themes} from '../../styles/colors.js';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import {AddressCopiedModal, QrCodeModal} from '../../components/BambooModals';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import {BigQrCode, Copy, Paste, QrCode} from '../../components/BambooSvgs';
import textStyles, {BambooTextStyles} from '../../styles/textStyles';
import {
  CopyAddressButton,
  ShowQrButton,
} from '../../components/BambooFooterItem.js';
import {ThemeContext} from '../../contexts/ThemeStore.js';
import {BlueStorageContext} from '../../blue_modules/storage-context.js';

const localeFile = require('../../locale.json');
const strings = new LocalizedStrings(localeFile);

export default function ReceiveScreen({navigation, route}) {
  const {wallets, selectedWallet} = React.useContext(BlueStorageContext);
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const wallet = wallets.find(w => selectedWallet === w.getID());

  const [address, setAddress] = React.useState('');

  React.useEffect(() => {
    console.log('useeffect receivescreen');
    const getAddress = async () => {
      return await wallet.getAddressAsync();
    };
    getAddress().then(address => {
      console.log('Setting address to: ' + address);
      setAddress(address);
    });
  }, [wallet]);

  const [qrCodeModalVisible, setQrCodeModalVisible] = React.useState(false);
  const toggleQrCodeModal = () => {
    setQrCodeModalVisible(!qrCodeModalVisible);
  };

  const [addressCopiedModalVisible, setAddressCopiedModalVisible] =
    React.useState(false);
  const toggleAddressCopiedModal = () => {
    setAddressCopiedModalVisible(!addressCopiedModalVisible);
  };

  const getAddressBox = () => {
    return (
      <BambooGradient>
        <BambooHeader
          navigation={navigation}
          title={strings.screens.receiveScreen.title}
        />

        <Text
          selectable
          style={[
            BambooTextStyles().regularTextStyle,
            {
              paddingTop: 32,
              marginHorizontal: 32,
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'flex-start',
            },
          ]}>
          {strings.formatString(strings.screens.receiveScreen.body, address)}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}>
          <CopyAddressButton toggleModal={toggleAddressCopiedModal} />
          <ShowQrButton toggleModal={toggleQrCodeModal} />
        </View>
        <QrCodeModal
          toggleModal={toggleQrCodeModal}
          isVisible={qrCodeModalVisible}
          address={address}
        />
        <AddressCopiedModal
          toggleModal={toggleAddressCopiedModal}
          isVisible={addressCopiedModalVisible}
          address={address}
        />
      </BambooGradient>
    );
  };

  return getAddressBox();
}
