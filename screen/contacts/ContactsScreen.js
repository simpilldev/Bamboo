import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Animated,
  StyleSheet,
} from 'react-native';

import Swipeable from 'react-native-gesture-handler/Swipeable';

import {RecyclerListView, DataProvider, LayoutProvider} from 'recyclerlistview';
import colors, {themes} from '../../styles/colors';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import {
  AddressesExistsModal,
  InvalidAddressModal,
  NewContactModal,
  UnknownAddressModal,
} from '../../components/BambooModals';
import {Add, Bin, Pencil, Send} from '../../components/BambooSvgs';
import {initialWindowMetrics} from 'react-native-safe-area-context';
import ContactsDatabaseHelper from '../../class/bamboo-contacts-storage';
import SQLite from 'react-native-sqlite-storage';
import {AddContactButton} from '../../components/BambooFooterItem';
import {ThemeContext} from '../../contexts/ThemeStore';
import LottieView from 'lottie-react-native';
import {BambooTextStyles} from '../../styles/textStyles';

const strings = new LocalizedStrings(require('../../locale.json'));

const ViewTypes = {
  FULL: 0,
};

const db = new ContactsDatabaseHelper();

export default function ContactsScreen({navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const [labels, setLabels] = React.useState([]);
  const [addresses, setAddresses] = React.useState([]);

  const [loading, setLoading] = React.useState(true);
  const [update, setUpdate] = React.useState(false);

  const toggleUpdate = () => {
    setUpdate(!update);
  };

  const [dataProvider, setDataProvider] = React.useState(
    new DataProvider((r1, r2) => {
      return r1 !== r2;
    }).cloneWithRows(addresses),
  );

  React.useEffect(() => {}, []);

  React.useEffect(() => {
    getContacts();
  }, [loading, update]);

  const getContacts = () => {
    db.getContacts().then(contacts => {
      contacts.addresses.forEach(element => {
        console.log('Address = ' + element);
      });

      setAddresses(contacts.addresses);
      setLabels(contacts.labels);
      setDataProvider(
        new DataProvider((r1, r2) => {
          return r1 !== r2;
        }).cloneWithRows(addresses),
      );
      setLoading(false);
      setUpdate(false);
    });
  };

  const {width} = Dimensions.get('window');

  const layoutProvider = new LayoutProvider(
    index => {
      return ViewTypes.FULL;
    },
    (type, dim) => {
      switch (type) {
        case ViewTypes.FULL:
          dim.width = width;
          dim.height = 120;
          break;
        default:
          dim.width = 0;
          dim.height = 0;
      }
    },
  );

  const rowRenderer = (type, data, index) => {
    const row = [];
    let prevOpenedRow;

    const closeRow = index => {
      console.log('closerow');
      if (prevOpenedRow && prevOpenedRow !== row[index]) {
        prevOpenedRow.close();
      }
      prevOpenedRow = row[index];
    };

    const renderLeftActions = (progress, dragX, onClick) => {
      const scale = dragX.interpolate({
        inputRange: [0, 100],
        outputRange: [0, 1],
      });
      return (
        <TouchableOpacity
          style={{
            marginStart: 32,
            width: '25%',
            height: '100%',
            justifyContent: 'center',
          }}
          onPress={() => {
            if (new ContactsDatabaseHelper().deleteContact(addresses[index])) {
              setUpdate(true);
            } else {
              console.error('Unable to delete contact :(');
            }
          }}>
          <Animated.View
            style={{
              backgroundColor: colors.swipeableBin,
              borderRadius: 20,
              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              width: 80,
              height: 80,
            }}>
            <Bin />
          </Animated.View>
        </TouchableOpacity>
      );
    };

    const renderRightActions = (progress, dragX) => {
      const scale = dragX.interpolate({
        inputRange: [-100, 0],
        outputRange: [1, 0],
      });

      return (
        <TouchableOpacity
          style={{
            marginEnd: 32,
            width: '25%',
            height: '100%',
            justifyContent: 'center',
          }}>
          <Animated.View
            style={{
              paddingLeft: 4,
              backgroundColor: colors.swipeablePencil,
              borderRadius: 20,
              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              width: 80,
              height: 80,
            }}>
            <Pencil />
          </Animated.View>
        </TouchableOpacity>
      );
    };

    return (
      <View style={{}}>
        <Swipeable
          overshootLeft={false}
          overshootRight={false}
          useNativeAnimations
          friction={1.5}
          renderRightActions={(progress, dragX) =>
            renderRightActions(progress, dragX)
          }
          renderLeftActions={(progress, dragX) =>
            renderLeftActions(progress, dragX)
          }
          onSwipeableOpen={() => closeRow(index)}
          ref={ref => (row[index] = ref)}
          style={{
            marginTop: 16,
            height: 100,
          }}>
          <View
            style={{
              height: 100,
              paddingVertical: 16,
              marginHorizontal: 32,
              justifyContent: 'space-around',
              borderRadius: 20,
              backgroundColor: colors.boxColor,
              flexDirection: 'row',
              alignItems: 'center',
              shadowColor: colors.shadowColor,
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.17,
              shadowRadius: 2,
              elevation: 2.5,
            }}
            onLongPress={toggleContextMenuModal}>
            <View
              style={{
                flex: 0.75,
                flexDirection: 'column',
              }}>
              <Text
                style={{
                  color: 'white',
                  marginBottom: 8,
                  marginLeft: 16,
                  fontFamily: 'Montserrat-Medium',
                  fontSize: 24,
                }}>
                {labels[index]}
              </Text>
              <Text
                style={{
                  color: 'white',
                  marginTop: 8,
                  marginLeft: 16,
                  fontFamily: 'Montserrat-Light',
                  fontSize: 14,
                }}
                numberOfLines={1}>
                {addresses[index]}
              </Text>
            </View>
            <View
              style={{
                flex: 0.25,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity>
                <Send />
              </TouchableOpacity>
            </View>
          </View>
        </Swipeable>
      </View>
    );
  };

  const getRecyclerView = () => {
    if (dataProvider.getSize() === 0) {
      return (
        <View
          style={{
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center',
            alignContent: 'center',
            flex: 1,
          }}>
          <Text
            style={[
              BambooTextStyles().regularTextStyle,
              {textAlign: 'center'},
            ]}>
            {strings.screens.contactsScreen.noContacts}
          </Text>
          <LottieView
            source={require('../../lottie/8852-searching-for-word.json')}
            style={{
              height: 200,
              width: 200,
            }}
            hardwareAccelerationAndroid
            cacheStrategy="strong"
            renderMode="HARDWARE"
            autoPlay
            loop
          />
          <AddContactButton toggleModal={toggleAddContactModal} />
        </View>
      );
    }
    if (loading || update) {
      return null;
    } else {
      return (
        <View
          style={{
            flex: 1,
            marginTop: 32,
          }}>
          <RecyclerListView
            layoutProvider={layoutProvider}
            dataProvider={dataProvider}
            rowRenderer={rowRenderer}
          />
          <AddContactButton toggleModal={toggleAddContactModal} isFooter />
        </View>
      );
    }
  };

  const [contextMenuModalVisible, setContextMenuModalVisible] =
    React.useState(false);
  const toggleContextMenuModal = () => {
    setContextMenuModalVisible(!contextMenuModalVisible);
  };

  const [errorModalVisible, setErrorModalVisible] = React.useState(false);
  const toggleErrorModal = () => {
    setErrorModalVisible(!errorModalVisible);
  };

  const [addContactModalVisible, setAddContactModalVisible] =
    React.useState(false);
  const toggleAddContactModal = () => {
    setAddContactModalVisible(!addContactModalVisible);
  };

  const [addressExistsModalVisible, setAddressExistsModalVisible] =
    React.useState(false);
  const toggleAddressExistsModal = () => {
    setAddressExistsModalVisible(!addressExistsModalVisible);
  };

  const fetchAddresses = () => {
    return addresses;
  };

  return (
    <BambooGradient>
      <BambooHeader
        navigation={navigation}
        title={strings.screens.contactsScreen.title}
      />

      {getRecyclerView()}

      <InvalidAddressModal
        toggleModal={toggleErrorModal}
        isVisible={errorModalVisible}
      />
      <AddressesExistsModal
        toggleModal={toggleAddressExistsModal}
        isVisible={addressExistsModalVisible}
      />
      <NewContactModal
        toggleModal={toggleAddContactModal}
        isVisible={addContactModalVisible}
        toggleUpdate={toggleUpdate}
        toggleErrorModal={toggleErrorModal}
        toggleAddressExistsModal={toggleAddressExistsModal}
        fetchAddresses={fetchAddresses}
      />
    </BambooGradient>
  );
}
