import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
  TextInput,
} from 'react-native';
import colors, {themes} from '../../styles/colors';
import * as React from 'react';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import LocalizedStrings from 'react-native-localization';
import {getPndPrice} from '../../components/CoinGeckoHelper';
import {SelectCurrencyModal} from '../../components/BambooModals';
import {Coins} from '../../components/BambooSvgs';
import {RoundingModes} from 'js-big-decimal/dist/node/roundingModes';
import textStyles from '../../styles/textStyles';
import {initialWindowMetrics} from 'react-native-safe-area-context';
import {SelectCurrencyButton} from '../../components/BambooFooterItem';
import {ThemeContext} from '../../contexts/ThemeStore';

const localeFile = require('../../locale.json');
const strings = new LocalizedStrings(localeFile);

const BigDecimal = require('js-big-decimal');

export default function ConvertScreen({navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const [currency, setCurrency] = React.useState('USD');
  const [price, setPrice] = React.useState('');

  const [pndValue, setPndValue] = React.useState('0.00');
  const [currencyValue, setCurrencyValue] = React.useState('0.00');

  const [update, setUpdate] = React.useState(false);

  const [selectCurrencyModalVisible, setSelectCurrencyModalVisible] =
    React.useState(false);
  const toggleSelectCurrencyModal = () => {
    setSelectCurrencyModalVisible(!selectCurrencyModalVisible);
  };

  React.useEffect(() => {
    getPndPrice(currency)
      .then(price => {
        console.log('Currency = ' + currency);
        console.log('Price = ' + price);
        setPrice(price);
        setUpdate(false);
      })
      .catch(error => {
        console.log(error);
      });
  }, [currency]);

  const convertPndToOtherCurrency = (amount, fetchedPrice) => {
    if (typeof fetchedPrice === 'undefined') fetchedPrice = price;

    let convertedAmount;
    try {
      convertedAmount = new BigDecimal(amount.replaceAll(',', '')).multiply(
        new BigDecimal(fetchedPrice),
      );
      convertedAmount.compareTo(new BigDecimal(0.001)) === -1 &&
      convertedAmount.compareTo(new BigDecimal(0)) !== 0
        ? (convertedAmount = convertedAmount
            .round(8, BigDecimal.RoundingModes.HALF_UP)
            .getPrettyValue())
        : (convertedAmount = convertedAmount
            .round(2, BigDecimal.RoundingModes.HALF_UP)
            .getPrettyValue());
      setCurrencyValue(convertedAmount);
    } catch (error) {
      console.warn(error);
    }
  };

  const convertOtherCurrencyToPnd = amount => {
    let convertedAmount;
    try {
      convertedAmount = new BigDecimal(amount.replaceAll(',', '')).divide(
        new BigDecimal(price),
      );
      console.log('Converted Amount = ' + convertedAmount.getValue());
      convertedAmount.compareTo(new BigDecimal(10)) === -1 &&
      convertedAmount.compareTo(new BigDecimal(0)) !== 0
        ? (convertedAmount = convertedAmount
            .round(8, BigDecimal.RoundingModes.HALF_UP)
            .getPrettyValue())
        : (convertedAmount = convertedAmount
            .round(2, BigDecimal.RoundingModes.HALF_UP)
            .getPrettyValue());
      setPndValue(convertedAmount);
    } catch (error) {
      console.warn(error);
    }
  };

  return (
    <BambooGradient>
      <BambooHeader
        navigation={navigation}
        title={strings.screens.convertScreen.title}
      />

      <View style={styles.centerStyle}>
        <View style={styles.pandacoinContainer}>
          <Text style={styles.tickerStyle}>PND</Text>
          <TextInput
            textAlign="center"
            defaultValue={pndValue}
            onChangeText={value => {
              convertPndToOtherCurrency(value);
            }}
            style={{
              width: '100%',
              color: 'white',
              marginVertical: 16,
              marginRight: 40,
              borderRadius: 10,
              backgroundColor: colors.boxColor,
              textAlign: 'center',
              fontFamily: 'Montserrat-Regular',
              fontSize: 18,
              shadowColor: colors.shadowColor,
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.17,
              shadowRadius: 2,
              elevation: 2.5,
            }}
            autoCapitalize="none"
            keyboardType="numeric"
            returnKeyType="next"
            underlineColorAndroid="#f000"
            blurOnSubmit={false}
          />
        </View>
        <View style={styles.otherCurrencyContainer}>
          <Text style={styles.tickerStyle}>{currency.toUpperCase()}</Text>
          <TextInput
            textAlign="center"
            defaultValue={currencyValue}
            onChangeText={value => {
              convertOtherCurrencyToPnd(value);
            }}
            style={{
              width: '100%',
              color: 'white',
              marginVertical: 16,
              marginRight: 40,
              borderRadius: 10,
              backgroundColor: colors.boxColor,
              textAlign: 'center',
              fontFamily: 'Montserrat-Regular',
              fontSize: 18,
              shadowColor: colors.shadowColor,
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.17,
              shadowRadius: 2,
              elevation: 2.5,
            }}
            autoCapitalize="none"
            keyboardType="numeric"
            returnKeyType="next"
            underlineColorAndroid="#f000"
            blurOnSubmit={false}
          />
        </View>
        <SelectCurrencyButton toggleModal={toggleSelectCurrencyModal} />
      </View>

      <SelectCurrencyModal
        toggleModal={toggleSelectCurrencyModal}
        isVisible={selectCurrencyModalVisible}
        setCurrency={setCurrency}
        convertPndToOtherCurrency={convertPndToOtherCurrency}
        pndValue={pndValue}
      />
    </BambooGradient>
  );
}

const styles = StyleSheet.create({
  centerStyle: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 32,
  },

  tickerStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 18,
    alignSelf: 'center',
    color: 'white',
  },

  pandacoinContainer: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 16,
  },

  otherCurrencyContainer: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: 16,
    marginBottom: 48,
  },
});
