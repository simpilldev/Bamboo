import {Dimensions, Text, TouchableOpacity, View} from 'react-native';
import * as React from 'react';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import {ThemeContext} from '../../contexts/ThemeStore';
import {themes} from '../../styles/colors';
import {DataProvider, LayoutProvider, RecyclerListView} from 'recyclerlistview';
import {BlueStorageContext} from '../../blue_modules/storage-context';
import {Swipeable} from 'react-native-gesture-handler';
import {Wallet, WalletDrawer} from '../../components/BambooSvgs';
import {HDSegwitBech32Wallet} from '../../class';
import {AddWalletButton} from '../../components/BambooFooterItem';
import LocalizedStrings from 'react-native-localization';
import {
  AddingWalletModal,
  WalletLabel,
  WalletLabelModal,
} from '../../components/BambooModals';
import {getFormattedWalletBalance} from '../../class/bamboo-currency';

const strings = new LocalizedStrings(require('../../locale.json'));

export default function WalletsScreen({navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const {wallets, setSelectedWallet} = React.useContext(BlueStorageContext);
  const [loading, setLoading] = React.useState(false);
  const [update, setUpdate] = React.useState(false);

  const [walletLabelModal, setWalletLabelModal] = React.useState(false);
  const toggleWalletLabelModal = () => {
    setWalletLabelModal(!walletLabelModal);
  };
  const [addingWalletModalVisible, setAddingWalletModalVisible] =
    React.useState(false);
  const toggleAddingWalletModal = () => {
    setAddingWalletModalVisible(!addingWalletModalVisible);
  };

  const {width} = Dimensions.get('window');

  const dataProvider = new DataProvider((r1, r2) => {
    return r1 !== r2;
  }).cloneWithRows(wallets);

  const layoutProvider = new LayoutProvider(
    index => {
      return 0;
    },
    (type, dim) => {
      dim.width = width;
      dim.height = 200;
    },
  );

  const rowRenderer = (type, data, index) => {
    const row = [];
    let prevOpenedRow;

    const closeRow = index => {
      console.log('closerow');
      if (prevOpenedRow && prevOpenedRow !== row[index]) {
        prevOpenedRow.close();
      }
      prevOpenedRow = row[index];
    };

    return (
      <TouchableOpacity
        style={{
          height: 180,
          paddingVertical: 16,
          marginHorizontal: 32,
          justifyContent: 'center',
          borderRadius: 10,
          backgroundColor: colors.boxColor,
          flexDirection: 'row',
          alignItems: 'center',
          shadowColor: colors.shadowColor,
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.17,
          shadowRadius: 2,
          elevation: 2.5,
        }}
        onPress={async () => {
          await wallets[index].fetchBalance();
          await wallets[index].fetchTransactions();
          setSelectedWallet(wallets[index].getID());
        }}>
        <View
          style={{
            height: 126,
            flex: 0.75,
            justifyContent: 'space-between',
            flexDirection: 'column',
            marginRight: 16,
          }}>
          <Text
            ellipsizeMode="tail"
            style={{
              color: 'white',
              fontFamily: 'Montserrat-Medium',
              fontSize: 24,
            }}>
            {wallets[index].getLabel()}
          </Text>
          <Text
            style={{
              color: colors.primaryTextColor,
              marginTop: 8,
              fontFamily: 'Montserrat-Light',
              fontSize: 14,
            }}
            numberOfLines={1}>
            {getFormattedWalletBalance(wallets[index].getBalance())} PND
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const getRecyclerView = () => {
    console.log('DataProvider size = ' + dataProvider.getSize());
    if (loading || update || dataProvider.getSize() === 0) {
      return null;
    } else {
      return (
        <View
          style={{
            flex: 1,
            marginTop: 32,
            paddingBottom: 32,
          }}>
          <RecyclerListView
            dataProvider={dataProvider}
            layoutProvider={layoutProvider}
            rowRenderer={rowRenderer}
          />
        </View>
      );
    }
  };

  const [label, setLabel] = React.useState('');

  return (
    <BambooGradient>
      <BambooHeader
        navigation={navigation}
        title={strings.screens.walletsScreen.title}
      />
      {getRecyclerView()}
      <AddWalletButton toggleModal={toggleWalletLabelModal} />
      <WalletLabel
        isVisible={walletLabelModal}
        toggleModal={toggleWalletLabelModal}
        label={label}
        setLabel={setLabel}
        onFinish={toggleAddingWalletModal}
      />
      <AddingWalletModal
        label={label}
        isVisible={addingWalletModalVisible}
        toggleModal={toggleAddingWalletModal}
      />
    </BambooGradient>
  );
}
