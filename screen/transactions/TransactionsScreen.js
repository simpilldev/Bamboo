import {Dimensions, Text, TouchableOpacity, View, _View} from 'react-native';
import * as React from 'react';
import BambooGradient from '../../components/BambooGradient';
import BambooHeader from '../../components/BambooHeader';
import {ThemeContext} from '../../contexts/ThemeStore';
import {themes} from '../../styles/colors';
import LocalizedStrings from 'react-native-localization';
import {DataProvider, LayoutProvider, RecyclerListView} from 'recyclerlistview';
import {BlueStorageContext} from '../../blue_modules/storage-context';
import {formatBalanceWithoutSuffix, transactionTimeToReadable} from '../../loc';
import {getFormattedWalletBalance} from '../../class/bamboo-currency';
import {Send} from '../../components/BambooSvgs';
const strings = new LocalizedStrings(require('../../locale.json'));

export default function TransactionsScreen({navigation}) {
  const {wallets, selectedWallet, txMetadata} =
    React.useContext(BlueStorageContext);
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;
  const wallet = wallets.find(w => selectedWallet === w.getID());

  const [loading, setLoading] = React.useState(true);

  const [transactions, setTransactions] = React.useState([]);

  React.useEffect(() => {
    setLoading(true);
    let txs = wallet.getTransactions();
    for (const tx of txs) {
      tx.sort_ts = +new Date(tx.received);
    }
    txs = txs.sort(function (a, b) {
      return b.sort_ts - a.sort_ts;
    });
    setTransactions(txs.slice(0, Infinity));
    setDataProvider(
      new DataProvider((r1, r2) => {
        return r1 === r2;
      }).cloneWithRows(transactions),
    );
    console.log('Txs length in useeffect = ' + txs.length);
    console.log(dataProvider.getSize());
    setLoading(false);
  }, [wallet, dataProvider]);

  const [dataProvider, setDataProvider] = React.useState(
    new DataProvider((r1, r2) => {
      return r1 === r2;
    }).cloneWithRows(transactions),
  );

  const fetchTxs = async () => {
    return await wallet.fetchTransactions();
  };
  const {width} = Dimensions.get('window');

  const layoutProvider = new LayoutProvider(
    index => {
      return 0;
    },
    (type, dim) => {
      dim.height = 120;
      dim.width = width;
    },
  );

  const rowRenderer = (type, data, index) => {
    const row = [];
    let prevOpenedRow;

    const closeRow = index => {
      console.log('closerow');
      if (prevOpenedRow && prevOpenedRow !== row[index]) {
        prevOpenedRow.close();
      }
      prevOpenedRow = row[index];
    };

    const renderLeftActions = (progress, dragX, onClick) => {
      const scale = dragX.interpolate({
        inputRange: [0, 100],
        outputRange: [0, 1],
      });
      return (
        <TouchableOpacity
          style={{
            marginStart: 32,
            width: '25%',
            height: '100%',
            justifyContent: 'center',
          }}
          onPress={() => {
            if (new ContactsDatabaseHelper().deleteContact(addresses[index])) {
              setUpdate(true);
            } else {
              console.error('Unable to delete contact :(');
            }
          }}>
          <Animated.View
            style={{
              backgroundColor: colors.swipeableBin,
              borderRadius: 20,
              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              width: 80,
              height: 80,
            }}>
            <Bin />
          </Animated.View>
        </TouchableOpacity>
      );
    };

    const renderRightActions = (progress, dragX) => {
      const scale = dragX.interpolate({
        inputRange: [-100, 0],
        outputRange: [1, 0],
      });

      return (
        <TouchableOpacity
          style={{
            marginEnd: 32,
            width: '25%',
            height: '100%',
            justifyContent: 'center',
          }}>
          <Animated.View
            style={{
              paddingLeft: 4,
              backgroundColor: colors.swipeablePencil,
              borderRadius: 20,
              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              width: 80,
              height: 80,
            }}>
            <Pencil />
          </Animated.View>
        </TouchableOpacity>
      );
    };

    return (
      <View
        style={{
          height: 100,
          paddingVertical: 16,
          marginHorizontal: 32,
          justifyContent: 'space-around',
          borderRadius: 20,
          backgroundColor: colors.boxColor,
          flexDirection: 'row',
          alignItems: 'center',
          shadowColor: colors.shadowColor,
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.17,
          shadowRadius: 2,
          elevation: 2.5,
        }}>
        <View
          style={{
            flex: 0.75,
            flexDirection: 'column',
          }}>
          <Text
            style={{
              color: 'white',
              marginBottom: 8,
              marginLeft: 16,
              fontFamily: 'Montserrat-Medium',
              fontSize: 24,
            }}>
            {getFormattedWalletBalance(transactions[index].value)} PND
          </Text>
          <Text
            style={{
              color: 'white',
              marginTop: 8,
              marginLeft: 16,
              fontFamily: 'Montserrat-Light',
              fontSize: 14,
            }}
            numberOfLines={1}>
            {transactionTimeToReadable(transactions[index].received)}
          </Text>
        </View>
        <View
          style={{
            flex: 0.25,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity>
            <Send />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const getRecyclerView = () => {
    if (!loading) {
      return (
        <RecyclerListView
          dataProvider={dataProvider}
          layoutProvider={layoutProvider}
          rowRenderer={rowRenderer}
        />
      );
    }
  };

  return (
    <BambooGradient>
      <BambooHeader
        navigation={navigation}
        title={strings.screens.txsScreen.title}
      />
      {getRecyclerView()}
    </BambooGradient>
  );
}
