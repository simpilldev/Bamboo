import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  ActivityIndicator,
  useColorScheme,
  LayoutAnimation,
} from 'react-native';
import {Icon} from 'react-native-elements';
import Biometric from '../../class/biometrics';
import LottieView from 'lottie-react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StackActions, useNavigation, useRoute} from '@react-navigation/native';
import {BlueStorageContext} from '../../blue_modules/storage-context';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {isHandset} from '../../blue_modules/environment';
import BambooHeader from '../../components/BambooHeader';
import BambooGradient from '../../components/BambooGradient';
import {themes} from '../../styles/colors';
import {ThemeContext} from '../../contexts/ThemeStore';
import SystemNavigationBar from 'react-native-system-navigation-bar';
import {BambooWallet} from '../../class/wallets/BambooWallet';
import {navigate} from '../../NavigationService';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AppStorage} from '../../class';
import OnAppLaunch from '../../class/on-app-launch';
const BlueElectrum = require('../../blue_modules/BlueElectrum');

export default function UnlockScreen({navigation}) {
  const {isDarkMode, setDarkMode} = React.useContext(ThemeContext);
  const colors = isDarkMode ? themes.dark : themes.light;

  const {
    setWalletsInitialized,
    isStorageEncrypted,
    startAndDecrypt,
    wallets,
    setSelectedWallet,
  } = useContext(BlueStorageContext);
  const [biometricType, setBiometricType] = useState(false);
  const [isStorageEncryptedEnabled, setIsStorageEncryptedEnabled] =
    useState(false);
  const [isAuthenticating, setIsAuthenticating] = useState(false);
  const [animationDidFinish, setAnimationDidFinish] = useState(false);
  const colorScheme = useColorScheme();

  const initialRender = async () => {
    let biometricType = false;
    if (await Biometric.isBiometricUseCapableAndEnabled()) {
      biometricType = await Biometric.biometricType();
    }

    setBiometricType(biometricType);
  };

  useEffect(() => {
    initialRender();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const changeNav = async () => {
      await SystemNavigationBar.setNavigationColor(
        colors.backgroundGradientBottom,
        true,
      );
    };
    changeNav();
  });

  const successfullyAuthenticated = async () => {
    setWalletsInitialized(true);
    if (doesBambooWalletExist()) {
      const defaultSelectedWallet =
        await OnAppLaunch.getSelectedDefaultWallet();
      console.log('Default = ' + defaultSelectedWallet.getID());
      setSelectedWallet(defaultSelectedWallet.getID());

      await BlueElectrum.connectMain();

      const addressesToTest = [
        'PWdoUrPx9F2jeZpQGhFs5jr5CDmuE98Foe',
        'PFsCrAMEvEiWB8xKDE8XBDqg9tKGLaQvNa',
        'PE5B8kUQYvyr664GECpEZTVTA2epCeGZoX',
        'PK36WDt9AV58ZzYtBcix239sk9BgoR2tUA',
        'PQvTQ59f4QdBdJ8FfwynTNXkMfmnjvWCEy',
        'PQQ97JUb7iQsjPk51XXpqrJZbmukWQWExF',
      ];

      await defaultSelectedWallet.fetchTransactions();
      await defaultSelectedWallet.fetchBalance();

      navigation.replace('HomeStack');
    } else navigation.replace('IntroStack');
  };

  const unlockWithBiometrics = async () => {
    if (await isStorageEncrypted()) {
      unlockWithKey();
    }
    setIsAuthenticating(true);

    if (await Biometric.unlockWithBiometrics()) {
      setIsAuthenticating(false);
      await startAndDecrypt();
      return successfullyAuthenticated();
    }
    setIsAuthenticating(false);
  };

  const unlockWithKey = async () => {
    setIsAuthenticating(true);
    if (await startAndDecrypt()) {
      ReactNativeHapticFeedback.trigger('notificationSuccess', {
        ignoreAndroidSystemSettings: false,
      });
      successfullyAuthenticated();
    } else {
      setIsAuthenticating(false);
    }
  };

  const renderUnlockOptions = () => {
    if (isAuthenticating) {
      return <ActivityIndicator />;
    } else {
      const color = colorScheme === 'dark' ? '#FFFFFF' : '#000000';
      if (
        (biometricType === Biometric.TouchID ||
          biometricType === Biometric.Biometrics) &&
        !isStorageEncryptedEnabled
      ) {
        return (
          <TouchableOpacity
            accessibilityRole="button"
            disabled={isAuthenticating}
            onPress={unlockWithBiometrics}>
            <Icon
              name="fingerprint"
              size={64}
              type="font-awesome5"
              color={color}
            />
          </TouchableOpacity>
        );
      } else if (
        biometricType === Biometric.FaceID &&
        !isStorageEncryptedEnabled
      ) {
        return (
          <TouchableOpacity
            accessibilityRole="button"
            disabled={isAuthenticating}
            onPress={unlockWithBiometrics}>
            <Image
              source={
                colorScheme === 'dark'
                  ? require('../../img/faceid-default.png')
                  : require('../../img/faceid-dark.png')
              }
              style={styles.icon}
            />
          </TouchableOpacity>
        );
      } else if (isStorageEncryptedEnabled) {
        return (
          <TouchableOpacity
            accessibilityRole="button"
            disabled={isAuthenticating}
            onPress={unlockWithKey}>
            <Icon name="lock" size={64} type="font-awesome5" color={color} />
          </TouchableOpacity>
        );
      }
    }
  };

  const doesBambooWalletExist = () => {
    return wallets.length > 0;
  };

  const onLayout = async () => {
    console.log('Does Wallet Exist = ' + doesBambooWalletExist());
    const storageIsEncrypted = await isStorageEncrypted();
    setIsStorageEncryptedEnabled(storageIsEncrypted);
    if (!biometricType || storageIsEncrypted) {
      unlockWithKey();
    } else if (typeof biometricType === 'string') unlockWithBiometrics();
  };

  return (
    <BambooGradient>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        <LottieView
          source={require('../../lottie/lottie_splash_wallet_loading.json')}
          style={{
            height: 200,
            width: 200,
          }}
          hardwareAccelerationAndroid
          cacheStrategy="strong"
          renderMode="HARDWARE"
          autoPlay
          loop
          onLayout={onLayout}
        />
      </View>
    </BambooGradient>
    // <SafeAreaView style={styles.root}>
    //   <StatusBar barStyle="default" />
    //   <View style={styles.container}>
    //     <LottieView
    //       source={require('./img/bluewalletsplash.json')}
    //       autoPlay
    //       loop={false}
    //       onAnimationFinish={onAnimationFinish}
    //     />
    //     <View style={styles.biometric}>
    //       {animationDidFinish && (
    //         <View style={styles.biometricRow}>{renderUnlockOptions()}</View>
    //       )}
    //     </View>
    //   </View>
    // </SafeAreaView>
  );
}
