import 'react-native-gesture-handler'; // should be on top
import React, {useContext, useEffect, useRef} from 'react';
import {
  AppState,
  NativeModules,
  NativeEventEmitter,
  Platform,
  UIManager,
  useColorScheme,
} from 'react-native';
import {NavigationContainer, DarkTheme} from '@react-navigation/native';
import {BlueStorageContext} from './blue_modules/storage-context';
import ContactsDatabaseHelper from './class/bamboo-contacts-storage';
import RootStack from './Navigation';
import {ThemeStore} from './contexts/ThemeStore';
import Theme from './components/BambooTheme';
import RNBootSplash from 'react-native-bootsplash';

const eventEmitter =
  Platform.OS === 'ios'
    ? new NativeEventEmitter(NativeModules.EventEmitter)
    : undefined;
const {EventEmitter} = NativeModules;

const ClipboardContentType = Object.freeze({
  BITCOIN: 'BITCOIN',
  LIGHTNING: 'LIGHTNING',
});

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

const App = () => {
  useEffect(() => {
    RNBootSplash.hide({fade: true});
    new ContactsDatabaseHelper().createTableIfEmpty();
  }, []);

  return (
    // Using dark theme gets rid of screen flashing during navigation.
    <ThemeStore>
      <Theme>
        <NavigationContainer theme={DarkTheme}>
          <RootStack />
        </NavigationContainer>
      </Theme>
    </ThemeStore>
  );
};

export default App;
